package com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.stadium

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.App
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import kotlinx.android.synthetic.main.lineup_stadium_rv_layout.view.*

class HomeStadiumRecyclerViewAdapter(
    private val homePlayers: MutableList<MatchModel.Lineup.Home.StartingLineups>,
    private val cards: MutableList<MatchModel.Cards>
) :
    RecyclerView.Adapter<HomeStadiumRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.lineup_stadium_rv_layout, parent, false)
        )

    override fun getItemCount() = homePlayers.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var homeModel: MatchModel.Lineup.Home.StartingLineups
        fun onBind() {
            homeModel = homePlayers[adapterPosition]
            itemView.lineupPlayersTV.text = homeModel.lineupPlayer
            itemView.lineupPlayersButton.text = homeModel.lineupNumber
            itemView.lineupPlayersButton.setBackgroundResource(R.mipmap.shirt_1)
            itemView.lineupPlayersButton.setTextColor(ContextCompat.getColor(App.context!!,android.R.color.white))
            cards.forEach {
                if (it.card.contains("yellow card") && it.homeFault.isNotEmpty()
                    && it.homeFault.contains(homeModel.lineupPlayer)
                ) {
                    itemView.lineupFoulCardImageView.setImageResource(R.mipmap.ic_card_yellow)
                } else if (it.card.contains("red card") && it.homeFault.isNotEmpty() &&
                    it.homeFault.contains(homeModel.lineupPlayer)
                ) {
                    itemView.lineupFoulCardImageView.setImageResource(R.mipmap.ic_card_red)
                }
            }
        }
    }

}