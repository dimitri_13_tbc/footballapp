package com.footballapp.app.ui.main.club.tab_layouts.players

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.R
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.HomeActivity
import kotlinx.android.synthetic.main.fragment_players.view.*

class PlayersFragment : BaseFragment() {

    private lateinit var adapter: PlayersRecyclerViewAdapter
    private var goalKeepers = 0
    private var defenders = 0
    private var midfielders = 0
    private var forwards = 0
    override fun layoutResource() = R.layout.fragment_players

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }

    private fun init(){
        itemView!!.playersRecyclerView.layoutManager = LinearLayoutManager(context)
        HomeActivity.clubModel.players.forEach {
            when (it.playerType) {
                "Defenders" -> defenders++
                "Goalkeepers" -> goalKeepers++
                "Midfielders" -> midfielders++
                "Forwards" -> forwards++
            }
        }
        if (HomeActivity.clubModel.coaches.size > 1){
            HomeActivity.clubModel.coaches.removeAt(0)
        }
        adapter = PlayersRecyclerViewAdapter(HomeActivity.clubModel,goalKeepers,defenders,midfielders,forwards)
        itemView!!.playersRecyclerView.adapter = adapter
    }

}