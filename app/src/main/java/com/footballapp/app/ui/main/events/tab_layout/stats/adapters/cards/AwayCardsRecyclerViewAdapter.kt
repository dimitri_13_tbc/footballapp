package com.footballapp.app.ui.main.events.tab_layout.stats.adapters.cards

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.event_away_player_info_recyclerview_item_layout.view.*
import kotlinx.android.synthetic.main.event_home_player_info_recyclerview_item_layout.view.*

class AwayCardsRecyclerViewAdapter(private val cards: MutableList<MatchModel.Cards>) :
    RecyclerView.Adapter<AwayCardsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_away_player_info_recyclerview_item_layout, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = cards.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: MatchModel.Cards
        fun onBind() {
            model = cards[adapterPosition]
            if (model.card.contains("yellow card"))
                itemView.awayCardImageView.setImageResource(R.mipmap.ic_card_yellow)
            else{
                itemView.awayCardImageView.setImageResource(R.mipmap.ic_card_red)
            }
            itemView.awayCardImageView.visible(true)
            itemView.awayTeamNumberShirtButton.visible(false)
            itemView.awayTeamScoreTimeTextView.text = model.getCardTime()
            itemView.awayTeamNameTextView.text = model.awayFault
        }
    }
}