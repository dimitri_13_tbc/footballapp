package com.footballapp.app.callbacks

import com.footballapp.app.models.MatchModel

interface MatchRecyclerViewInterface {
    fun onMatchClick(matchModel: MatchModel)
}