package com.footballapp.app.ui.main.competitions.tab_layouts.standings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.standing_team_recyclerview_item_layout.view.*

class TeamStandingRecyclerViewAdapter(private val items: MutableList<StandingModel>) :
    RecyclerView.Adapter<TeamStandingRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.standing_team_recyclerview_item_layout, parent, false
        )
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: StandingModel
        fun onBind() {

            model = items[adapterPosition]
            if (model.overallLeaguePosition.toInt() <= 4) {
                itemView.standingColor.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        android.R.color.holo_green_light
                    )
                )
            } else if (model.overallLeaguePosition.toInt() == 5 || model.overallLeaguePosition.toInt() == 6) {
                itemView.standingColor.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.colorYellow
                    )
                )
            } else if (model.overallLeaguePosition.toInt() == items.size || model.overallLeaguePosition.toInt() == items.size - 1 || model.overallLeaguePosition.toInt() == items.size - 2) {
                itemView.standingColor.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        android.R.color.holo_red_dark
                    )
                )
            } else {
                itemView.standingColor.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.colorPrimary
                    )
                )
            }

            if (model.overallLeaguePosition.toInt() % 2 == 1) {
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.colorPrimaryDark
                    )
                )
            }else {
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.standingItemBackgroundColor
                    ))
            }

            itemView.clubTextView.text = model.teamName
            itemView.rankTextView.text = model.overallLeaguePosition
            Glide.with(itemView.context).load(model.teamBadge).into(itemView.clubImageView)
        }
    }

}