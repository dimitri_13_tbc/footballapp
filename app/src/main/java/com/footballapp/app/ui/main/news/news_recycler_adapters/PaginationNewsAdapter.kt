package com.footballapp.app.ui.main.news.news_recycler_adapters

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.ui.main.news.NewsModel
import kotlinx.android.synthetic.main.news_recyclerview_item_layout.view.*


class PaginationNewsAdapter(
    private val recyclerView: RecyclerView,
    private val items: MutableList<NewsModel.Articles>,
    val onLoadMoreListener: OnLoadMoreListener,
    val activity: Activity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val LOADING_ITEM = 2
        const val LIST_ITEM = 1
    }


    private var isLoading = false
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private val visibleThreshold = 4

    init {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()

                if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                    onLoadMoreListener.onLoadMore()
                    isLoading = true

                }
            }
        })
    }


    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder)
            holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: NewsModel.Articles
        fun onBind() {
            model = items[adapterPosition]

            if (model.urlToImage != null){
                itemView.newsImageView.setGlideImage(model.urlToImage!!)
            }else{
                itemView.newsImageView.setImageResource(R.mipmap.ic_news_red)
            }

            itemView.newsDateTextView.text = model.getDateWithWord()
            itemView.newsTitleTextView.text = model.title
            itemView.fullInfoContainer.visibility =
                if (model.isFullInfo) View.VISIBLE else View.GONE
            itemView.fullTitleTextView.text = model.title
            itemView.fullDescriptionTextView.text = model.description

            itemView.setOnClickListener {
                model.isFullInfo = !model.isFullInfo
                notifyItemChanged(adapterPosition)
            }
            itemView.newsUrlButton.setOnClickListener {
                val uri: Uri = Uri.parse("googlechrome://navigate?url=${model.url}")
                val i = Intent(Intent.ACTION_VIEW, uri)
                if (i.resolveActivity(activity.packageManager) == null) {
                    i.data = Uri.parse(model.url)
                }
                activity.startActivity(i)
            }
        }
    }

    inner class LoadMorePostsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setLoaded() {
        isLoading = false
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            items[position].isLast -> {
                LOADING_ITEM
            }
            else -> {
                LIST_ITEM
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            LIST_ITEM -> {
                ViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.news_recyclerview_item_layout, parent, false)
                )
            }
            else -> {
                LoadMorePostsViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.news_recyclerview_load_more_layout, parent, false)
                )
            }
        }
    }


}
