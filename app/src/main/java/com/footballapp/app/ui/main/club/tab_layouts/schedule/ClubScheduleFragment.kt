package com.footballapp.app.ui.main.club.tab_layouts.schedule

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.R
import com.footballapp.app.callbacks.MatchRecyclerViewInterface
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.FootballDataLoader
import com.footballapp.app.services.network.Endpoints
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.models.MatchModel
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.tools.Tools
import com.footballapp.app.ui.main.adapters.ScheduleMatchesRecyclerViewAdapter
import com.footballapp.app.ui.main.events.EventContainerFragment
import com.footballapp.app.ui.main.events.ScheduledMatchesPreviewFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_bottom_navigation.*
import kotlinx.android.synthetic.main.fragment_club_schedule.view.*
import org.json.JSONArray

class ClubScheduleFragment : BaseFragment(), MatchRecyclerViewInterface {

    private lateinit var adapter: ScheduleMatchesRecyclerViewAdapter
    private val items = mutableListOf<MatchModel>()
    private lateinit var homeActivity: HomeActivity


    override fun layoutResource() = R.layout.fragment_club_schedule

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        homeActivity = activity as HomeActivity
        init()
    }

    private fun init() {
        itemView!!.clubScheduleRecyclerView.layoutManager = LinearLayoutManager(context)
        adapter =
            ScheduleMatchesRecyclerViewAdapter(
                items
                , this
            )
        itemView!!.clubScheduleRecyclerView.adapter = adapter
        getClubSchedule()
    }

    private fun getClubSchedule() {
        val currentDates = Tools.getCurrentDates()
        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_EVENTS
        parameters["from"] = currentDates.getValue("from")
        parameters["to"] = currentDates.getValue("to")
        parameters["team_id"] = HomeActivity.clubModel.teamKey
        parameters["APIkey"] = SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        FootballDataLoader.getRequest(null, parameters, object : CustomCallback {
            override fun onResponse(response: String) {
                if (response[0] == '[') {
                    val list = Gson().fromJson(response, Array<MatchModel>::class.java).toList()
                    list.forEach {
                        items.add(it)
                    }
                    adapter.notifyDataSetChanged()
                }
            }
        })

    }

    override fun onMatchClick(matchModel: MatchModel) {
        d("clickeddd1", matchModel.matchStatus)
        if (matchModel.matchStatus.contains("Finished")) {
            val beginTransaction = homeActivity.supportFragmentManager.beginTransaction()
            beginTransaction.add(
                R.id.fragmentContainer,
                EventContainerFragment(
                    matchModel
                )
            )
            beginTransaction.addToBackStack("EVENT")
            beginTransaction.commit()
            homeActivity.fragmentContainer.visibility = View.VISIBLE
            homeActivity.viewPager.visibility = View.INVISIBLE
        }else{
            val beginTransaction = homeActivity.supportFragmentManager.beginTransaction()
            beginTransaction.add(
                R.id.fragmentContainer,
                ScheduledMatchesPreviewFragment(
                    matchModel
                )
            )
            beginTransaction.addToBackStack("EVENT")
            beginTransaction.commit()
            homeActivity.fragmentContainer.visibility = View.VISIBLE
            homeActivity.viewPager.visibility = View.INVISIBLE
        }

    }

}