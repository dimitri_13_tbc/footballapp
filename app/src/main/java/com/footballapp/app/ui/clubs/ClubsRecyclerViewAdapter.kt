package com.footballapp.app.ui.clubs

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.callbacks.CustomClickRV
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.main_recyclerview_item_layout.view.*
import java.util.*

class ClubsRecyclerViewAdapter(
    private val items: MutableList<ClubsModel>,
    val customClickRV: CustomClickRV
) : RecyclerView.Adapter<ClubsRecyclerViewAdapter.ViewHolder>() , Filterable {
    var filterItems = mutableListOf<ClubsModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.main_recyclerview_item_layout, parent, false
        )
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: ClubsModel
        fun onBind(){
            model = items[adapterPosition]
            Glide.with(itemView.context).load(model.teamBadge).into(itemView.logoImageView)
            itemView.titleTextView.text = model.teamName
            itemView.setOnClickListener {
                customClickRV.itemClick(model.teamKey)
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val resultList = mutableListOf<ClubsModel>()
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    resultList.addAll(filterItems)
                } else {
                    for (item in filterItems) {
                        if (item.teamName.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(
                                Locale.ROOT))) {
                            resultList.add(item)
                        }
                    }

                }
                val filterResults = FilterResults()
                filterResults.values = resultList
                return  filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                items.clear()
                items.addAll(results!!.values as Collection<ClubsModel>)
                notifyDataSetChanged()
            }

        }
    }
}