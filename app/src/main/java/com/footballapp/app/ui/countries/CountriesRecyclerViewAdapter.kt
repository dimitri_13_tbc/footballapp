package com.footballapp.app.ui.countries

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.footballapp.app.R
import com.footballapp.app.callbacks.CustomClickRV
import kotlinx.android.synthetic.main.main_recyclerview_item_layout.view.*
import java.util.*

class CountriesRecyclerViewAdapter(private val items: MutableList<CountriesModel>,val customClickRV: CustomClickRV) :
    RecyclerView.Adapter<CountriesRecyclerViewAdapter.ViewHolder>()  , Filterable{

    var filterItems = mutableListOf<CountriesModel>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.main_recyclerview_item_layout, parent, false
        )
    )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: CountriesModel
        fun onBind() {
            model = items[adapterPosition]
            Glide.with(itemView.context).load(model.countryLogo).into(itemView.logoImageView)
            itemView.titleTextView.text = model.countryName
            itemView.setOnClickListener {
                customClickRV.itemClick(model.countryId)
            }
        }
    }


    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val resultList = mutableListOf<CountriesModel>()
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    resultList.addAll(filterItems)
                } else {
                    for (item in filterItems) {
                        if (item.countryName.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(item)
                        }
                    }

                }
                val filterResults = FilterResults()
                filterResults.values = resultList
                return  filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                items.clear()
                items.addAll(results!!.values as Collection<CountriesModel>)
                notifyDataSetChanged()
            }

        }
    }


}