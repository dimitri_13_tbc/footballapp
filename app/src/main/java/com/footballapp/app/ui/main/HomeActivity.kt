package com.footballapp.app.ui.main

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.Tools.dpToPx
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.ui.main.chat.ChatFragment
import com.footballapp.app.ui.main.account.AccountFragment
import com.footballapp.app.ui.main.club.ClubFragment
import com.footballapp.app.ui.main.competitions.CompetitionsFragment
import com.footballapp.app.ui.clubs.ClubModel
import com.footballapp.app.ui.main.news.NewsContainerFragment
import kotlinx.android.synthetic.main.activity_bottom_navigation.*

class HomeActivity : AppCompatActivity() {


    companion object {
        var clubModel = ClubModel()
        var userActive = false
        var nextMatchModel = MatchModel()
        var lastMatchModel = MatchModel()
        var lastMatchHomeTeamBitmap: Bitmap? = null
        var lastMatchAwayTeamBitmap: Bitmap? = null
        var nextMatchHomeTeamBitmap: Bitmap? = null
        var nextMatchAwayTeamBitmap: Bitmap? = null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)
        init()
    }

    private fun init() {
        val fragments = mutableListOf<Fragment>()
        fragments.add(CompetitionsFragment())
        fragments.add(NewsContainerFragment())
        fragments.add(ClubFragment())
        fragments.add(ChatFragment(clubModel))
        fragments.add(AccountFragment(clubModel))

        viewPager.adapter =
            MainViewPagerAdapter(
                supportFragmentManager,
                fragments
            )
        viewPager.offscreenPageLimit = fragments.size

        navView.itemIconTintList = null
        navView.menu.findItem(R.id.navClub).title = clubModel.teamName


        bottomNavigationClubLogoButton.setGlideImage(clubModel.teamBadge)
        bottomNavigationClubLogoButton.animate().translationY(dpToPx(-20))

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                navView.menu.getItem(position).isChecked = true
            }
        })

        navView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navCompetitions -> {
                    checkFragmentVisibility()
                    viewPager.currentItem = 0
                    bottomNavigationClubLogoButton.animate().translationY(0f)
                }
                R.id.navNews -> {
                    checkFragmentVisibility()
                    viewPager.currentItem = 1
                    bottomNavigationClubLogoButton.animate().translationY(0f)
                }
                R.id.navClub -> {
                    checkFragmentVisibility()
                    viewPager.currentItem = 2
                    bottomNavigationClubLogoButton.animate().translationY(dpToPx(-20))
                }
                R.id.navChat -> {
                    checkFragmentVisibility()
                    viewPager.currentItem = 3
                    bottomNavigationClubLogoButton.animate().translationY(0f)
                }
                R.id.navAccount -> {
                    checkFragmentVisibility()
                    viewPager.currentItem = 4
                    bottomNavigationClubLogoButton.animate().translationY(0f)
                }

            }
            true
        }

        if (userActive) {
            navView.menu.getItem(4).isChecked = true
            viewPager.currentItem = 4
            bottomNavigationClubLogoButton.animate().translationY(0f)
            userActive = false
        } else {
            navView.menu.getItem(2).isChecked = true
            viewPager.currentItem = 2
        }

    }

    private fun checkFragmentVisibility() {
        if (viewPager.visibility == View.INVISIBLE) {
            viewPager.visibility = View.VISIBLE
            fragmentContainer.visibility = View.GONE
            supportFragmentManager.popBackStack(
                "EVENT",
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
        }
    }


}