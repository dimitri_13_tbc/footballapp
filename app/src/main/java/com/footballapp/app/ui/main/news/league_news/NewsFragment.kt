package com.footballapp.app.ui.main.news.league_news

import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.App
import com.footballapp.app.R
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.NewsDataLoader
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.news.NewsModel
import com.footballapp.app.ui.main.news.news_recycler_adapters.OnLoadMoreListener
import com.footballapp.app.ui.main.news.news_recycler_adapters.PaginationNewsAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_news.view.*
import org.json.JSONObject


class NewsFragment : BaseFragment() {

    private lateinit var adapter: PaginationNewsAdapter

    private var newsList = mutableListOf<NewsModel.Articles>()

    var count = 2

    override fun layoutResource() = R.layout.fragment_news
    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }


    private fun init() {

        itemView!!.newsRecyclerView.layoutManager = LinearLayoutManager(context)

        adapter =
            PaginationNewsAdapter(
                itemView!!.newsRecyclerView,
                newsList,
                loadMoreListener,
                activity!!
            )
        itemView!!.newsRecyclerView.adapter = adapter
        itemView!!.newsSwipeRefresh.setOnRefreshListener {
            getNewsData()
        }
        getNewsData()

    }

    private fun getNewsData() {
        val parameters = mutableMapOf<String, String>()
        parameters["qInTitle"] =
            SharedPreference.instance().getString(SharedPreference.LEAGUE_NAME)!!
        parameters["apiKey"] = App.context!!.getString(R.string.NEWS_API_KEY)
        parameters["language"] = "en"
        NewsDataLoader.getRequest(null, parameters, object : CustomCallback {
            override fun onResponse(response: String) {
                newsList.clear()
                val json = JSONObject(response)
                if (json.has("status") && json.getString("status") == "ok") {
                    val toMutableList =
                        Gson().fromJson(response, NewsModel::class.java)
                    toMutableList.articles.forEach {
                        newsList.add(it)
                    }
                    newsList.sortByDescending {
                        it.publishedAt
                    }
                }
                adapter.notifyDataSetChanged()
                itemView!!.newsSwipeRefresh.isRefreshing = false
            }

            override fun onFailure(title: String, errorMessage: String) {

            }
        })
    }

    private val loadMoreListener = object :
        OnLoadMoreListener {
        override fun onLoadMore() {
            if (newsList.size != 0) {
                if (!newsList[newsList.size - 1].isLast) {
                    itemView!!.newsRecyclerView.post {
                        if (count < 6) {
                            val newsModel =
                                NewsModel.Articles()
                            newsModel.isLast = true
                            newsList.add(newsModel)
                            adapter.notifyItemInserted(newsList.size - 1)
                            Handler().postDelayed({ getMoreNews(count) }, 1000)

                        }
                    }
                }
            }
        }
    }

    private fun getMoreNews(page: Int) {
        val parameters = mutableMapOf<String, String>()
        parameters["qInTitle"] =
            SharedPreference.instance().getString(SharedPreference.LEAGUE_NAME)!!
        parameters["apiKey"] = App.context!!.getString(R.string.NEWS_API_KEY)
        parameters["language"] = "en"
        parameters["page"] = page.toString()
        NewsDataLoader.getRequest(null, parameters, object : CustomCallback {
            override fun onResponse(response: String) {

                count++

                val lastPosition = newsList.size
                if (lastPosition > 0) {
                    newsList.removeAt(newsList.size - 1)
                    adapter.notifyItemRemoved(newsList.size - 1)
                }
                adapter.setLoaded()
                val json = JSONObject(response)
                if (json.has("status") && json.getString("status") == "ok") {
                    val toMutableList =
                        Gson().fromJson(response, NewsModel::class.java)
                    toMutableList.articles.sortByDescending {
                        it.publishedAt
                    }
                    toMutableList.articles.forEach {
                        newsList.add(it)
                    }

                }
                if (newsList.isNotEmpty() && newsList.size != 1) adapter.notifyItemMoved(
                    lastPosition,
                    newsList.size - 1
                )
                adapter.notifyDataSetChanged()
            }

            override fun onFailure(title: String, errorMessage: String) {

            }
        })
    }


}