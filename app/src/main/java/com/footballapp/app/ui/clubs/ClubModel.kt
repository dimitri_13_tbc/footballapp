package com.footballapp.app.ui.clubs

import com.google.gson.annotations.SerializedName

class ClubModel  {
    @SerializedName("team_key")
    val teamKey = ""

    @SerializedName("team_name")
    val teamName = ""

    @SerializedName("team_badge")
    val teamBadge = ""

    val players = mutableListOf<Players>()
    val coaches = mutableListOf<Coaches>()


    class Coaches  {
        @SerializedName("coach_name")
        val coachName = ""

        @SerializedName("coach_country")
        val coachCountry = ""

        @SerializedName("coach_age")
        val coachAge = ""
    }


    class Players  {
        @SerializedName("player_key")
        val playerKey: Long? = null

        @SerializedName("player_name")
        val playerName = ""

        @SerializedName("player_number")
        val playerNumber = ""

        @SerializedName("player_country")
        val playerCountry = ""

        @SerializedName("player_type")
        val playerType = ""

        @SerializedName("player_age")
        val playerAge = ""

        @SerializedName("player_match_played")
        val playerMatchPlayed = ""

        @SerializedName("player_goals")
        val playerGoals = ""

        @SerializedName("player_yellow_cards")
        val playerYellowCards = ""

        @SerializedName("player_red_cards")
        val playerRedCards = ""

        var isFullInfo = false
    }
}