package com.footballapp.app.ui.main.events.tab_layout.stats.adapters.goals

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.event_home_player_info_recyclerview_item_layout.view.*

class HomeTeamGoalScorersRecyclerViewAdapter(private val items: List<MatchModel.Goalscorer>) :
    RecyclerView.Adapter<HomeTeamGoalScorersRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_home_player_info_recyclerview_item_layout, parent, false)
        )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: MatchModel.Goalscorer
        fun onBind(){
            model = items[adapterPosition]
            itemView.homeTeamScoreTimeTextView.text = model.getGoalTime()
            itemView.homeTeamNameTextView.text = model.homeScorer
            itemView.homeTeamNumberShirtButton.visible(false)
            itemView.homeBallImageView.visible(true)
//            itemView.homePlayersContainer.setBackgroundResource(android.R.color.transparent)
        }
    }

}