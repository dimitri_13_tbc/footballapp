package com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.missing_players

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.event_home_player_info_recyclerview_item_layout.view.*

class HomeTeamMissingPlayersRecyclerViewAdapter(private val items: List<MatchModel.Lineup.Home.MissingPlayers>) :
    RecyclerView.Adapter<HomeTeamMissingPlayersRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_home_player_info_recyclerview_item_layout, parent, false)
        )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: MatchModel.Lineup.Home.MissingPlayers
        fun onBind(){
            model = items[adapterPosition]
            itemView.homeTeamNameTextView.text = model.lineupPlayers
            itemView.homeTeamScoreTimeTextView.visible(false)
            if (adapterPosition % 2 == 0) {
                itemView.homePlayersContainer.setBackgroundResource(R.color.transparentGray)
            } else {
                itemView.homePlayersContainer.setBackgroundResource(android.R.color.transparent)
            }
        }
    }




}