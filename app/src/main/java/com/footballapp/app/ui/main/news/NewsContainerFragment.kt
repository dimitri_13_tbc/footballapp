package com.footballapp.app.ui.main.news

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import com.footballapp.app.R
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.news.league_news.NewsFragment
import com.footballapp.app.ui.main.news.videos.VideosFragment
import kotlinx.android.synthetic.main.fragment_news_container.view.*

class NewsContainerFragment : BaseFragment() {

    lateinit var adapter: NewsContainerViewPagerAdapter

    override fun layoutResource() = R.layout.fragment_news_container

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }

    private fun init(){
        val fragments = listOf<Fragment>(NewsFragment(),VideosFragment())
        adapter = NewsContainerViewPagerAdapter(childFragmentManager,fragments)
        itemView!!.newsContainerViewPager.adapter = adapter
        itemView!!.newsContainerTabLayout.setupWithViewPager(itemView!!.newsContainerViewPager)
    }

}