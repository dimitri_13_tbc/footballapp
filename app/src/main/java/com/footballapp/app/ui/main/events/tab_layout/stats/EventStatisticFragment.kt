package com.footballapp.app.ui.main.events.tab_layout.stats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.R
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.ui.main.events.tab_layout.stats.adapters.cards.AwayCardsRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.stats.adapters.cards.HomeCardsRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.stats.adapters.goals.AwayTeamGoalScorersRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.stats.adapters.goals.HomeTeamGoalScorersRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.stats.adapters.statistic.MatchTimeLineModel
import com.footballapp.app.ui.main.events.tab_layout.stats.adapters.statistic.MatchTimelineRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.stats.adapters.statistic.StatisticRecyclerViewAdapter
import kotlinx.android.synthetic.main.event_lineup_title_toolbar_layout.view.*
import kotlinx.android.synthetic.main.fragment_event_statistic.view.*


class EventStatisticFragment(private val matchModel: MatchModel) : BaseFragment() {

    companion object {
        const val GOAL = "goal"
        const val SUB = "substitution"
        const val YELLOW_CARD = "yellow card"
        const val RED_CARD = "red card"
    }

    private lateinit var homeGoalsAdapter: HomeTeamGoalScorersRecyclerViewAdapter
    private lateinit var awayGoalsAdapter: AwayTeamGoalScorersRecyclerViewAdapter
    private lateinit var statisticAdapter: StatisticRecyclerViewAdapter
    private lateinit var timelineAdapter: MatchTimelineRecyclerViewAdapter

    private lateinit var homeCardsRecyclerViewAdapter: HomeCardsRecyclerViewAdapter
    private lateinit var awayCardsRecyclerViewAdapter: AwayCardsRecyclerViewAdapter

    private val homeCards = mutableListOf<MatchModel.Cards>()
    private val awayCards = mutableListOf<MatchModel.Cards>()
    private val homeGoalScorersList = mutableListOf<MatchModel.Goalscorer>()
    private val awayGoalScorersList = mutableListOf<MatchModel.Goalscorer>()
    private val timelineList = mutableListOf<MatchTimeLineModel>()


    override fun layoutResource() = R.layout.fragment_event_statistic

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {

        init()
    }

    private fun init() {
        fillGoalLists()
        initRecyclerViews()
        setData()


    }

    private fun fillGoalLists() {
        matchModel.goalscorer.forEach {
            if (it.homeScorer.isNotEmpty())
                homeGoalScorersList.add(it)
            else
                awayGoalScorersList.add(it)
        }
    }


    private fun initRecyclerViews() {
        itemView!!.homeTeamGoalScorersRecyclerView.layoutManager = LinearLayoutManager(context)
        homeGoalsAdapter = HomeTeamGoalScorersRecyclerViewAdapter(homeGoalScorersList)
        itemView!!.homeTeamGoalScorersRecyclerView.adapter = homeGoalsAdapter

        homeCardsRecyclerViewAdapter = HomeCardsRecyclerViewAdapter(homeCards)
        itemView!!.homeTeamCardsRecyclerView.layoutManager = LinearLayoutManager(context)
        itemView!!.homeTeamCardsRecyclerView.adapter = homeCardsRecyclerViewAdapter

        awayCardsRecyclerViewAdapter = AwayCardsRecyclerViewAdapter(awayCards)
        itemView!!.awayTeamCardsRecyclerView.layoutManager = LinearLayoutManager(context)
        itemView!!.awayTeamCardsRecyclerView.adapter = awayCardsRecyclerViewAdapter

        itemView!!.awayTeamGoalScorersRecyclerView.layoutManager = LinearLayoutManager(context)
        awayGoalsAdapter = AwayTeamGoalScorersRecyclerViewAdapter(awayGoalScorersList)
        itemView!!.awayTeamGoalScorersRecyclerView.adapter = awayGoalsAdapter

        itemView!!.statisticRecyclerView.layoutManager = LinearLayoutManager(context)
        statisticAdapter = StatisticRecyclerViewAdapter(matchModel.statistics)
        itemView!!.statisticRecyclerView.adapter = statisticAdapter

        setTimeline()
        itemView!!.matchTimeLineRecyclerView.layoutManager = LinearLayoutManager(context)
        timelineAdapter = MatchTimelineRecyclerViewAdapter(timelineList)
        itemView!!.matchTimeLineRecyclerView.adapter = timelineAdapter
    }

    private fun setData() {
        itemView!!.leagueImageView.setGlideImage(matchModel.leagueLogo)
        itemView!!.leagueTextView.text = matchModel.leagueName
        itemView!!.matchDayTextView.text = matchModel.matchRound
        itemView!!.homeTeamBadgeImageView.setGlideImage(matchModel.teamHomeBadge)
        itemView!!.awayTeamBadgeImageView.setGlideImage(matchModel.teamAwayBadge)
        itemView!!.homeTeamScoreTextView.text = matchModel.matchHomeTeamScore
        itemView!!.awayTeamScoreTextView.text = matchModel.matchAwayTeamScore
        itemView!!.takePlaceStadiumTV.text = matchModel.matchStadium
        itemView!!.toolbarHomeTeamImageView.setGlideImage(matchModel.teamHomeBadge)
        itemView!!.toolbarAwayTeamImageView.setGlideImage(matchModel.teamAwayBadge)
        itemView!!.toolbarTitleTV.text = getString(R.string.match_events)

        itemView!!.awayTeamNameStatisticTextView.text = matchModel.matchAwayTeamName
        itemView!!.homeTeamNameStatisticTextView.text = matchModel.matchHomeTeamName

        if (matchModel.cards.size > 0) {
            matchModel.cards.forEach {
                if (it.homeFault.isNotEmpty()) {
                    homeCards.add(it)
                } else if (it.awayFault.isNotEmpty()) {
                    awayCards.add(it)
                }
            }
        }
    }

    private fun setTimeline() {
        matchModel.goalscorer.forEach {
            if (it.homeScorer.isEmpty())
                timelineList.add(MatchTimeLineModel(it.time, it.awayScorer, GOAL, false))
            else
                timelineList.add(MatchTimeLineModel(it.time, it.homeScorer, GOAL, true))
        }

        matchModel.cards.forEach {
            if (it.homeFault.isEmpty())
                timelineList.add(MatchTimeLineModel(it.time, it.awayFault, it.card, false))
            else
                timelineList.add(MatchTimeLineModel(it.time, it.homeFault, it.card, true))
        }
        matchModel.substitutions.home.forEach {
            timelineList.add(MatchTimeLineModel(it.time, it.substitution, SUB, true))
        }
        matchModel.substitutions.away.forEach {
            timelineList.add(MatchTimeLineModel(it.time, it.substitution, SUB, false))
        }

        timelineList.sortBy {
            it.time
        }
    }

}