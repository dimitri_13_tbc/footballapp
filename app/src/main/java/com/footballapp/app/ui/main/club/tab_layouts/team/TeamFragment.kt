package com.footballapp.app.ui.main.club.tab_layouts.team

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.App
import com.footballapp.app.R
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.NewsDataLoader
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.news.news_recycler_adapters.OnLoadMoreListener
import com.footballapp.app.ui.main.news.news_recycler_adapters.PaginationNewsAdapter
import com.footballapp.app.ui.main.news.NewsModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_team.view.*
import org.json.JSONObject


class TeamFragment : BaseFragment() {

    private lateinit var clubNewsAdapter : PaginationNewsAdapter
    private val clubNewsList = mutableListOf<NewsModel.Articles>()
    var count = 2

    override fun layoutResource() = R.layout.fragment_team
    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }
    private fun init(){
        itemView!!.clubNewsRecyclerView.layoutManager = LinearLayoutManager(context)
        clubNewsAdapter =
            PaginationNewsAdapter(
                itemView!!.clubNewsRecyclerView,
                clubNewsList,
                loadMoreListener,
                activity!!
            )
        itemView!!.clubNewsRecyclerView.adapter = clubNewsAdapter
        itemView!!.clubNewsSwipeRefresh.setOnRefreshListener {
            getClubNews()
        }
        getClubNews()
    }

    private fun getClubNews(){
        val parameters = mutableMapOf<String,String>()
        parameters["qInTitle"] = HomeActivity.clubModel.teamName
        parameters["apiKey"] = getString(R.string.NEWS_API_KEY)
        parameters["language"] = "en"
        NewsDataLoader.getRequest(null,parameters,object : CustomCallback{
            override fun onResponse(response: String) {

                clubNewsList.clear()
                val json = JSONObject(response)
                if (json.has("status") && json.getString("status") == "ok"){
                    val toMutableList =
                        Gson().fromJson(response, NewsModel::class.java)

                    toMutableList.articles.forEach {
                        clubNewsList.add(it)
                    }
                    clubNewsList.sortByDescending {
                        it.publishedAt
                    }
                    clubNewsAdapter.notifyDataSetChanged()
                    itemView!!.clubNewsSwipeRefresh.isRefreshing = false
                }
            }
        })
    }
    private val loadMoreListener = object :
        OnLoadMoreListener {
        override fun onLoadMore() {
            if (clubNewsList.size != 0) {
                if (!clubNewsList[clubNewsList.size - 1].isLast) {
                    itemView!!.clubNewsRecyclerView.post {
                        if (count < 6) {
                            val newsModel = NewsModel.Articles()
                            newsModel.isLast = true
                            clubNewsList.add(newsModel)
                            clubNewsAdapter.notifyItemInserted(clubNewsList.size - 1)
                            Handler().postDelayed({ getMoreNews(count) }, 1000)

                        }


                    }
                }
            }
        }
    }

    private fun getMoreNews(page: Int) {
        val parameters = mutableMapOf<String, String>()
        parameters["qInTitle"] =
            SharedPreference.instance().getString(SharedPreference.LEAGUE_NAME)!!
        parameters["apiKey"] = App.context!!.getString(R.string.NEWS_API_KEY)
        parameters["language"] = "en"
        parameters["page"] = page.toString()
        NewsDataLoader.getRequest(null, parameters, object : CustomCallback {
            override fun onResponse(response: String) {
                count++

                val lastPosition = clubNewsList.size
                if (lastPosition > 0) {
                    clubNewsList.removeAt(clubNewsList.size - 1)
                    clubNewsAdapter.notifyItemRemoved(clubNewsList.size - 1)
                }
                clubNewsAdapter.setLoaded()
                val json = JSONObject(response)
                if (json.has("status") && json.getString("status") == "ok") {
                    val toMutableList =
                        Gson().fromJson(response, NewsModel::class.java)
                    toMutableList.articles.sortByDescending {
                        it.publishedAt
                    }
                    toMutableList.articles.forEach {
                        clubNewsList.add(it)
                    }


                }
                if (clubNewsList.isNotEmpty() && clubNewsList.size != 1) clubNewsAdapter.notifyItemMoved(
                    lastPosition,
                    clubNewsList.size - 1
                )
                clubNewsAdapter.notifyDataSetChanged()
            }

            override fun onFailure(title: String, errorMessage: String) {

            }
        })
    }


}