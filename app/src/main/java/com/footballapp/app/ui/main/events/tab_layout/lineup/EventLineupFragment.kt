package com.footballapp.app.ui.main.events.tab_layout.lineup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.footballapp.app.R
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.App
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.missing_players.AwayTeamMissingPlayersRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.missing_players.HomeTeamMissingPlayersRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.stadium.AwayStadiumRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.stadium.HomeStadiumRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.substitutes.AwayTeamSubstitutesRecyclerViewAdapter
import com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.substitutes.HomeTeamSubstitutesRecyclerViewAdapter
import kotlinx.android.synthetic.main.event_lineup_title_toolbar_layout.view.*
import kotlinx.android.synthetic.main.events_stadium_layout.view.*
import kotlinx.android.synthetic.main.events_stadium_layout.view.homeGoalkeeper
import kotlinx.android.synthetic.main.fragment_event_lineup.view.*
import kotlinx.android.synthetic.main.lineup_stadium_rv_layout.view.*


class EventLineupFragment(private val matchModel: MatchModel) : BaseFragment() {

    override fun layoutResource() = R.layout.fragment_event_lineup

    private lateinit var homeTeamSubstitutesAdapter: HomeTeamSubstitutesRecyclerViewAdapter
    private lateinit var awayTeamSubstitutesAdapter: AwayTeamSubstitutesRecyclerViewAdapter

    private lateinit var awayTeamMissingPlayersAdapter: AwayTeamMissingPlayersRecyclerViewAdapter
    private lateinit var homeTeamMissingPlayersAdapter: HomeTeamMissingPlayersRecyclerViewAdapter

    private lateinit var adapterHomeDefenders: HomeStadiumRecyclerViewAdapter
    private lateinit var adapterHomeMidfielders: HomeStadiumRecyclerViewAdapter
    private lateinit var adapterHomeForwards: HomeStadiumRecyclerViewAdapter

    private lateinit var adapterAwayDefenders: AwayStadiumRecyclerViewAdapter
    private lateinit var adapterAwayMidfielders: AwayStadiumRecyclerViewAdapter
    private lateinit var adapterAwayForwards: AwayStadiumRecyclerViewAdapter

    private val homeLineups = mutableListOf<MatchModel.Lineup.Home.StartingLineups>()
    private val awayLineups = mutableListOf<MatchModel.Lineup.Away.StartingLineups>()

    private val homeDefenders = mutableListOf<MatchModel.Lineup.Home.StartingLineups>()
    private val homeMidfielders = mutableListOf<MatchModel.Lineup.Home.StartingLineups>()
    private val homeForwards = mutableListOf<MatchModel.Lineup.Home.StartingLineups>()
    private val awayDefenders = mutableListOf<MatchModel.Lineup.Away.StartingLineups>()
    private val awayMidfielders = mutableListOf<MatchModel.Lineup.Away.StartingLineups>()
    private val awayForwards = mutableListOf<MatchModel.Lineup.Away.StartingLineups>()

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        initAdapters()
        initViews()
    }

    private fun initViews() {
        itemView!!.awayGoalkeeper.lineupPlayersButton.setTextColor(
            ContextCompat.getColor(context!!, R.color.colorPrimaryDark)
        )
        itemView!!.toolbarHomeTeamImageView.setGlideImage(matchModel.teamHomeBadge)
        itemView!!.toolbarAwayTeamImageView.setGlideImage(matchModel.teamAwayBadge)
        itemView!!.toolbarTitleTV.text = getString(R.string.substitutes)

        itemView!!.homeLineupImageView.setGlideImage(matchModel.teamHomeBadge)
        itemView!!.awayLineupImageView.setGlideImage(matchModel.teamAwayBadge)

        itemView!!.homeGoalkeeper.lineupPlayersButton.setBackgroundResource(R.mipmap.ic_away_shirt)
        itemView!!.homeGoalkeeper.lineupPlayersButton.text = homeLineups[0].lineupNumber
        itemView!!.homeGoalkeeper.lineupPlayersButton.setTextColor(ContextCompat.getColor(App.context!!,android.R.color.black))
        itemView!!.homeGoalkeeper.lineupPlayersTV.text = homeLineups[0].lineupPlayer


        itemView!!.awayGoalkeeper.lineupPlayersButton.setBackgroundResource(R.mipmap.ic_home_shirt)
        itemView!!.awayGoalkeeper.lineupPlayersButton.text = awayLineups[0].lineupNumber
        itemView!!.awayGoalkeeper.lineupPlayersButton.setTextColor(ContextCompat.getColor(App.context!!,android.R.color.white))
        itemView!!.awayGoalkeeper.lineupPlayersTV.text = awayLineups[0].lineupPlayer

        itemView!!.missingPlayersToolbar.toolbarHomeTeamImageView.setGlideImage(matchModel.teamHomeBadge)
        itemView!!.missingPlayersToolbar.toolbarAwayTeamImageView.setGlideImage(matchModel.teamAwayBadge)
        itemView!!.missingPlayersToolbar.toolbarTitleTV.text = getString(R.string.missing_players)
    }

    private fun initAdapters() {
        homeTeamMissingPlayersAdapter =
            HomeTeamMissingPlayersRecyclerViewAdapter(matchModel.lineup.home.missingPlayers)
        itemView!!.homeTeamMissingPlayersRecyclerView.layoutManager = LinearLayoutManager(context)
        itemView!!.homeTeamMissingPlayersRecyclerView.adapter = homeTeamMissingPlayersAdapter

        awayTeamMissingPlayersAdapter =
            AwayTeamMissingPlayersRecyclerViewAdapter(matchModel.lineup.away.missingPlayers)
        itemView!!.awayTeamMissingPlayersRecyclerView.layoutManager = LinearLayoutManager(context)
        itemView!!.awayTeamMissingPlayersRecyclerView.adapter = awayTeamMissingPlayersAdapter

        homeTeamSubstitutesAdapter =
            HomeTeamSubstitutesRecyclerViewAdapter(matchModel.lineup.home.substitutes)
        itemView!!.homeTeamSubstitutesRecyclerView.layoutManager = LinearLayoutManager(context)
        itemView!!.homeTeamSubstitutesRecyclerView.adapter = homeTeamSubstitutesAdapter

        awayTeamSubstitutesAdapter =
            AwayTeamSubstitutesRecyclerViewAdapter(matchModel.lineup.away.substitutes)
        itemView!!.awayTeamSubstitutesRecyclerView.layoutManager = LinearLayoutManager(context)
        itemView!!.awayTeamSubstitutesRecyclerView.adapter = awayTeamSubstitutesAdapter

        adapterHomeDefenders = HomeStadiumRecyclerViewAdapter(homeDefenders, matchModel.cards)
        itemView!!.homeDefendersRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        itemView!!.homeDefendersRV.adapter = adapterHomeDefenders

        adapterHomeMidfielders = HomeStadiumRecyclerViewAdapter(homeMidfielders, matchModel.cards)
        itemView!!.homeMidfieldersRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        itemView!!.homeMidfieldersRV.adapter = adapterHomeMidfielders

        adapterHomeForwards = HomeStadiumRecyclerViewAdapter(homeForwards, matchModel.cards)
        itemView!!.homeForwardsRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        itemView!!.homeForwardsRV.adapter = adapterHomeForwards

        adapterAwayDefenders = AwayStadiumRecyclerViewAdapter(awayDefenders, matchModel.cards)
        itemView!!.awayDefendersRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        itemView!!.awayDefendersRV.adapter = adapterAwayDefenders

        adapterAwayMidfielders = AwayStadiumRecyclerViewAdapter(awayMidfielders, matchModel.cards)
        itemView!!.awayMidfieldersRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        itemView!!.awayMidfieldersRV.adapter = adapterAwayMidfielders

        adapterAwayForwards = AwayStadiumRecyclerViewAdapter(awayForwards, matchModel.cards)
        itemView!!.awayForwardsRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        itemView!!.awayForwardsRV.adapter = adapterAwayForwards

        if (matchModel.lineup.home.startingLineups.size == 11) {
            setHomePlayers()
        }

        if (matchModel.lineup.away.startingLineups.size == 11) {
            setAwayPlayers()
        }
    }

    private fun setHomePlayers() {
        val homeTeamSystemSplit = matchModel.matchHomeTeamSystem.split(" - ")
        val hDefenders: Int
        val hForwards: Int
        val hMidfielders: Int

        if (homeTeamSystemSplit.size > 3) {
            hDefenders = homeTeamSystemSplit[0].toInt()


            when (homeTeamSystemSplit.size) {
                4 -> {
                    hMidfielders = homeTeamSystemSplit[1].toInt()
                    hForwards = homeTeamSystemSplit[2].toInt() + homeTeamSystemSplit[3].toInt()
                }
                5 -> {
                    hMidfielders = homeTeamSystemSplit[1].toInt() + homeTeamSystemSplit[2].toInt()
                    hForwards = homeTeamSystemSplit[3].toInt() + homeTeamSystemSplit[4].toInt()
                }
                else -> {
                    hMidfielders = homeTeamSystemSplit[1].toInt()
                    hForwards = homeTeamSystemSplit[2].toInt()
                }
            }
        } else {
            hDefenders = 4
            hForwards = 3
            hMidfielders = 3
        }

        itemView!!.homeLineupSystemTV.text = "$hDefenders - $hMidfielders - $hForwards"

        matchModel.lineup.home.startingLineups.forEach {
            homeLineups.add(it)
        }

        var checkHomePosition = 0
        homeLineups.forEach {
            if (it.lineupPosition.isNotEmpty())
                checkHomePosition++
        }
        if (checkHomePosition == 11) {
            homeLineups.sortBy { it.lineupPosition.toInt() }
        }

        for (each in 1..hDefenders) {
            homeDefenders.add(homeLineups[each])
        }
        for (each in hDefenders + 1..(hMidfielders + hDefenders)) {
            homeMidfielders.add(homeLineups[each])
        }
        for (each in hMidfielders + hDefenders + 1..(hForwards + hMidfielders + hDefenders)) {
            homeForwards.add(homeLineups[each])
        }

        adapterHomeDefenders.notifyDataSetChanged()
        adapterHomeMidfielders.notifyDataSetChanged()
        adapterHomeForwards.notifyDataSetChanged()
    }

    private fun setAwayPlayers() {
        val awayTeamSystemSplit = matchModel.matchAwayTeamSystem.split(" - ")

        val aDefenders: Int
        val aForwards: Int
        val aMidfielders: Int

        if (awayTeamSystemSplit.size > 3) {
            aDefenders = awayTeamSystemSplit[0].toInt()
            when (awayTeamSystemSplit.size) {
                4 -> {
                    aMidfielders = awayTeamSystemSplit[1].toInt()
                    aForwards = awayTeamSystemSplit[2].toInt() + awayTeamSystemSplit[3].toInt()
                }
                5 -> {
                    aMidfielders = awayTeamSystemSplit[1].toInt() + awayTeamSystemSplit[2].toInt()
                    aForwards = awayTeamSystemSplit[3].toInt() + awayTeamSystemSplit[4].toInt()
                }
                else -> {
                    aMidfielders = awayTeamSystemSplit[1].toInt()
                    aForwards = awayTeamSystemSplit[2].toInt()
                }
            }
        } else {
            aDefenders = 4
            aForwards = 3
            aMidfielders = 3
        }

        itemView!!.awayLineupSystemTV.text = "$aDefenders - $aMidfielders - $aForwards"

        matchModel.lineup.away.startingLineups.forEach {
            awayLineups.add(it)

        }
        var checkAwayPosition = 0
        awayLineups.forEach {
            if (it.lineupPosition.isNotEmpty())
                checkAwayPosition++
        }
        if (checkAwayPosition == 11) {
            awayLineups.sortBy { it.lineupPosition.toInt() }
        }

        for (each in 1..aDefenders) {
            awayDefenders.add(awayLineups[each])
        }
        for (each in aDefenders + 1..(aMidfielders + aDefenders)) {
            awayMidfielders.add(awayLineups[each])
        }
        for (each in aMidfielders + aDefenders + 1..(aForwards + aMidfielders + aDefenders)) {
            awayForwards.add(awayLineups[each])
        }

        adapterAwayDefenders.notifyDataSetChanged()
        adapterAwayMidfielders.notifyDataSetChanged()
        adapterAwayForwards.notifyDataSetChanged()

    }

}