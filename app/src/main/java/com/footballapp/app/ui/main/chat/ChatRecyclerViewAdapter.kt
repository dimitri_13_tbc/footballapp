package com.footballapp.app.ui.main.chat


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.chat_rv_layout.view.*

class ChatRecyclerViewAdapter(
    private val messages: MutableList<ChatModel>
) : RecyclerView.Adapter<ChatRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.chat_rv_layout, parent, false)
    )

    override fun getItemCount() = messages.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var chatModel: ChatModel
        fun onBind() {
            chatModel = messages[adapterPosition]
            itemView.chatDateTextView.text = chatModel.time
            itemView.chatUserImageView.setGlideImage(chatModel.profileImgUrl)
            itemView.chatUsernameTextView.text = chatModel.username
            if (chatModel.text.isNotEmpty()) {
                itemView.chatMessageTextView.visible(true)
                itemView.chatMessageTextView.text = chatModel.text
            } else {
                itemView.chatMessageTextView.visible(false)
            }
            itemView.favouriteClubImageView.setGlideImage(chatModel.favouriteClubUrl)
            if (chatModel.imageMsg.isNotEmpty()) {
                itemView.chatImageContainer.visible(true)
                itemView.chatImageView.setGlideImage(chatModel.imageMsg)
            } else {
                itemView.chatImageContainer.visible(false)
            }

        }
    }
}