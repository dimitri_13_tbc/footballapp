package com.footballapp.app.tools.extensions

import android.widget.ImageView
import com.footballapp.app.App
import com.bumptech.glide.Glide

fun ImageView.setGlideImage(img: String) {
    Glide.with(App.context!!).load(img).into(this)
}