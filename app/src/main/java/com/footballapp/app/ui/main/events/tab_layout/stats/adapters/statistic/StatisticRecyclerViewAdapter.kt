package com.footballapp.app.ui.main.events.tab_layout.stats.adapters.statistic

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import kotlinx.android.synthetic.main.event_statistics_recyclerview_item_layout.view.*


class StatisticRecyclerViewAdapter(private val items:List<MatchModel.Statistics>) : RecyclerView.Adapter<StatisticRecyclerViewAdapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_statistics_recyclerview_item_layout, parent, false)
        )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.onBind()
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        lateinit var model: MatchModel.Statistics
        fun onBind(){
            model = items[adapterPosition]
            itemView.statisticHomeTeamTextView.text = model.home
            itemView.statisticTitleTextView.text = model.type
            itemView.statisticAwayTeamTextView.text = model.away
            val home = LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT,model.home.replace('%',' ').toFloat()
            )
            val away = LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT,model.away.replace('%',' ').toFloat()
            )
            itemView.homeTeamStatsView.layoutParams = home
            itemView.awayTeamStatsView.layoutParams = away
        }
    }
}