package com.footballapp.app.ui.main.news.videos


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.ui.main.news.news_recycler_adapters.OnLoadMoreListener
import com.footballapp.app.ui.main.news.news_recycler_adapters.PaginationNewsAdapter
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import kotlinx.android.synthetic.main.videos_rv_layout.view.*

class VideosRecyclerViewAdapter(
    private val videos: MutableList<VideoModel>,
    val recyclerView: RecyclerView,
    val onLoadMoreListener: OnLoadMoreListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val LOADING_ITEM = 2
        const val LIST_ITEM = 1
    }


    private var isLoading = false
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private val visibleThreshold = 2

    init {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()

                if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                    onLoadMoreListener.onLoadMore()

                    isLoading = true

                }
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            PaginationNewsAdapter.LIST_ITEM -> {
                ViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.videos_rv_layout, parent, false)
                )
            }
            else -> {
                LoadMorePostsViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.news_recyclerview_load_more_layout, parent, false)
                )
            }
        }
    }

    override fun getItemCount() = videos.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder)
            holder.onBind()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: VideoModel
        fun onBind() {
            model = videos[adapterPosition]

            itemView.videoPlayer.getPlayerUiController().showFullscreenButton(false)
            itemView.videoPlayer.getPlayerUiController().showMenuButton(false)
            itemView.videoPlayer.getPlayerUiController().showYouTubeButton(false)
            itemView.videoPlayer.getPlayerUiController().showVideoTitle(false)

            itemView.videoPlayer.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer) {
                    itemView.videoNameTextView.text = model.videoName
                    val videoId = model.videoId
                    youTubePlayer.cueVideo(videoId, 0F)
                }
            })
        }
    }

    inner class LoadMorePostsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun setLoaded() {
        isLoading = false
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            videos[position].isLast -> {
                LOADING_ITEM
            }
            else -> {
                LIST_ITEM
            }
        }
    }
}