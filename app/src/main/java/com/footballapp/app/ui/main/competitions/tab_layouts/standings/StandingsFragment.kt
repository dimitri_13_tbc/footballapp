package com.footballapp.app.ui.main.competitions.tab_layouts.standings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.FootballDataLoader
import com.footballapp.app.services.network.Endpoints
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.ui.BaseFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_standings.view.*
import kotlinx.android.synthetic.main.progressbar_layout.*


class StandingsFragment : BaseFragment() {

    private val standingList = mutableListOf<StandingModel>()
    private lateinit var teamStandingAdapter: TeamStandingRecyclerViewAdapter
    private lateinit var tableStandingAdapter: TableStandingRecyclerViewAdapter


    override fun layoutResource() = R.layout.fragment_standings

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {

        init()
    }

    private fun init() {
        scrollRV()

        getStanding()
        initStandingsTableRecyclerView()
        initStandingsTeamRecyclerView()
    }

    private fun getStanding() {
        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_STANDINGS
        parameters["league_id"] = SharedPreference.instance().getString(SharedPreference.LEAGUE)!!
        parameters["APIkey"] = SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        FootballDataLoader.getRequest(spinKitContainer, parameters, object : CustomCallback {
            override fun onResponse(response: String) {
                if (response[0] == '['){
                    val toList = Gson().fromJson(response, Array<StandingModel>::class.java).toList()

                    toList.forEach {
                        standingList.add(it)
                    }

                    tableStandingAdapter.notifyDataSetChanged()
                    teamStandingAdapter.notifyDataSetChanged()
                }

            }
        })
    }

    private fun initStandingsTeamRecyclerView() {
        itemView!!.standingsTeamRecyclerView.layoutManager = LinearLayoutManager(context)
        teamStandingAdapter = TeamStandingRecyclerViewAdapter(standingList)
        itemView!!.standingsTeamRecyclerView.adapter = teamStandingAdapter

    }

    private fun initStandingsTableRecyclerView() {
        itemView!!.standingsTableRecyclerView.layoutManager = LinearLayoutManager(context)
        tableStandingAdapter = TableStandingRecyclerViewAdapter(standingList)
        itemView!!.standingsTableRecyclerView.adapter = tableStandingAdapter
    }

    private var draggingView = -1

    private fun scrollRV() {
        itemView!!.standingsTeamRecyclerView.addOnScrollListener(scrollListener)
        itemView!!.standingsTableRecyclerView.addOnScrollListener(scrollListener)
    }

    private val scrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (itemView!!.standingsTeamRecyclerView == recyclerView && newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    draggingView = 1
                } else if (itemView!!.standingsTableRecyclerView == recyclerView && newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    draggingView = 2
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (draggingView == 1 && recyclerView == itemView!!.standingsTeamRecyclerView) {
                    itemView!!.standingsTableRecyclerView.scrollBy(dx, dy)
                } else if (draggingView == 2 && recyclerView == itemView!!.standingsTableRecyclerView) {
                    itemView!!.standingsTeamRecyclerView.scrollBy(dx, dy)
                }
            }
        }

}

