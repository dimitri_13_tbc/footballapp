package com.footballapp.app.ui

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class CustomViewPager(context: Context, attrs: AttributeSet?) : ViewPager(context, attrs) {

    private var touchEnable = false

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return if (touchEnable)
            super.onTouchEvent(ev)
        else
            touchEnable
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return if (touchEnable)
            return super.onInterceptTouchEvent(ev)
        else
            touchEnable
    }


}