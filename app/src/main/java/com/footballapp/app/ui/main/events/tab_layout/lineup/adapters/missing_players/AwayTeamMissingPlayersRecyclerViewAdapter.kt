package com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.missing_players

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.event_away_player_info_recyclerview_item_layout.view.*

class AwayTeamMissingPlayersRecyclerViewAdapter(private val items: List<MatchModel.Lineup.Away.MissingPlayers>) :
    RecyclerView.Adapter<AwayTeamMissingPlayersRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_away_player_info_recyclerview_item_layout, parent, false)
        )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: MatchModel.Lineup.Away.MissingPlayers
        fun onBind(){
            model = items[adapterPosition]
            itemView.awayTeamNameTextView.text = model.lineupPlayers
            itemView.awayTeamScoreTimeTextView.visible(false)
            if (adapterPosition % 2 == 0) {
                itemView.awayPlayersContainer.setBackgroundResource(R.color.transparentGray)
            } else {
                itemView.awayPlayersContainer.setBackgroundResource(android.R.color.transparent)
            }
        }
    }
}