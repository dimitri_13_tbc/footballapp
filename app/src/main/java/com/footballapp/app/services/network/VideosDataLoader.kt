package com.footballapp.app.services.network

import android.util.Log
import android.view.View
import com.footballapp.app.App
import com.footballapp.app.R
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

object VideosDataLoader {
    private const val HTTP_200_OK = 200
    private const val HTTP_201_CREATED = 201
    private const val HTTP_400_BAD_REQUEST = 400
    private const val HTTP_401_UNAUTHORIZED = 401
    private const val HTTP_404_NOT_FOUND = 404
    private const val HTTP_500_INTERNAL_SERVER_ERROR = 500
    private const val HTTP_204_NO_CONTENT = 204


    private var retrofit = Retrofit.Builder()
        .baseUrl(App.context!!.getString(R.string.VIDEOS_BASE_URL))
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()


    private var service = retrofit.create(
        ApiService::class.java)

    fun getRequest(loadingView: View?, param: Map<String, String>?, customCallback: CustomCallback) {
        if (loadingView != null) {
            loadingView.visibility = View.VISIBLE
        }
        val call = service.getVideosRequest(param)
        call.enqueue(
            handleResponse(
                loadingView,
                customCallback
            )
        )
    }

    private fun handleResponse(
        loadingView: View? = null,
        customCallback: CustomCallback
    ): Callback<String> {
        return object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                if (loadingView != null)
                    loadingView.visibility = View.GONE

            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (loadingView != null)
                    loadingView.visibility = View.GONE
                if (response.code() == HTTP_200_OK || response.code() == HTTP_201_CREATED)
                    try {
                        Log.d("serverResponse", " success " + response.body()!!)
                        customCallback.onResponse(response.body()!!)
                    } catch (e: JSONException) {
                        customCallback.onFailure(
                            App.context!!.getString(R.string.incorrect_request),
                            App.context!!
                                .getString(R.string.an_error_occurred_please_try_again)
                        )
                    }
                else if (response.code() == HTTP_400_BAD_REQUEST)
                    handleError(
                        response.errorBody()!!.string(),
                        customCallback
                    )
                else if (response.code() == HTTP_401_UNAUTHORIZED) {
                    handleError(
                        response.errorBody()!!.string(),
                        customCallback
                    )
                } else if (response.code() == HTTP_404_NOT_FOUND)
                    handleError(
                        response.errorBody()!!.string(),
                        customCallback
                    )
                else if (response.code() == HTTP_500_INTERNAL_SERVER_ERROR)
                    handleError(
                        response.errorBody()!!.string(),
                        customCallback
                    )
                else if (response.code() == HTTP_204_NO_CONTENT)
                    handleError(
                        "",
                        customCallback
                    )
                else {
                    Log.d("serverResponse", " ${response.errorBody()!!.string()}")
                    customCallback.onFailure(
                        App.context!!.getString(R.string.incorrect_request),
                        App.context!!.getString(R.string.an_error_occurred_please_try_again)
                    )
                }
            }
        }
    }

    private fun handleError(errorBody: String, customCallback: CustomCallback) {
        val errorJson = JSONObject(errorBody)
        Log.d("errorJson", " $errorJson")
        if (errorJson.has("error")) {
            customCallback.onFailure(
                App.context!!.resources.getString(R.string.incorrect_request),
                errorJson.getString("error")
            )
        }
    }
}