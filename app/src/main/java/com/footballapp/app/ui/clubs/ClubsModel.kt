package com.footballapp.app.ui.clubs

import com.google.gson.annotations.SerializedName

class ClubsModel {
    @SerializedName("team_key")
    val teamKey = ""

    @SerializedName("team_name")
    val teamName = ""

    @SerializedName("team_badge")
    val teamBadge = ""
}