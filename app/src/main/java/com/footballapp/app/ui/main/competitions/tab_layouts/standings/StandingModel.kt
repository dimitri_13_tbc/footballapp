package com.footballapp.app.ui.main.competitions.tab_layouts.standings

import com.google.gson.annotations.SerializedName

class StandingModel {
    @SerializedName("country_name")
    val countryName = ""
    @SerializedName("league_id")
    val leagueId = ""
    @SerializedName("league_name")
    val leagueName = ""
    @SerializedName("team_id")
    val teamId = ""
    @SerializedName("team_name")
    val teamName = ""
    @SerializedName("overall_promotion")
    val overallPromotion = ""
    @SerializedName("overall_league_position")
    val overallLeaguePosition = ""
    @SerializedName("overall_league_payed")
    val overallLeaguePlayer = ""
    @SerializedName("overall_league_W")
    val overallLeagueW = ""
    @SerializedName("overall_league_D")
    val overallLeagueD = ""
    @SerializedName("overall_league_L")
    val overallLeagueL = ""
    @SerializedName("overall_league_GF")
    val overallLeagueGF = ""
    @SerializedName("overall_league_GA")
    val overallLeagueGA = ""
    @SerializedName("overall_league_PTS")
    val overallLeaguePTS = ""
    @SerializedName("home_league_position")
    val homeLeaguePosition = ""
    @SerializedName("home_promotion")
    val homePromotion = ""
    @SerializedName("home_league_payed")
    val homeLeaguePlayed = ""
    @SerializedName("home_league_W")
    val homeLeagueW = ""
    @SerializedName("home_league_D")
    val homeLeagueD = ""
    @SerializedName("home_league_L")
    val homeLeagueL = ""
    @SerializedName("home_league_GF")
    val homeLeagueGF = ""
    @SerializedName("home_league_GA")
    val homeLeagueGA = ""
    @SerializedName("home_league_PTS")
    val homeLeaguePTS = ""
    @SerializedName("away_league_position")
    val awayLeaguePosition = ""
    @SerializedName("away_promotion")
    val awayPromotion = ""
    @SerializedName("away_league_payed")
    val awayLeaguePlayer = ""
    @SerializedName("away_league_W")
    val awayLeagueW = ""
    @SerializedName("away_league_D")
    val awayLeagueD = ""
    @SerializedName("away_league_L")
    val awayLeagueL = ""
    @SerializedName("away_league_GF")
    val awayLeagueGF = ""
    @SerializedName("away_league_GA")
    val awayLeagueGA = ""
    @SerializedName("away_league_PTS")
    val awayLeaguePTS = ""
    @SerializedName("league_round")
    val leagueRound = ""
    @SerializedName("team_badge")
    val teamBadge = ""


}