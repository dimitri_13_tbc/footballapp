package com.footballapp.app.services.network

object Endpoints {
    const val GET_COUNTRIES = "get_countries"
    const val GET_LEAGUES = "get_leagues"
    const val GET_TEAMS = "get_teams"
    const val GET_STANDINGS = "get_standings"
    const val GET_EVENTS = "get_events"

}