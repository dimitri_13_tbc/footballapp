package com.footballapp.app.tools

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.util.Patterns
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import com.footballapp.app.App
import com.footballapp.app.R
import kotlinx.android.synthetic.main.error_dialog_layout.*
import java.text.SimpleDateFormat
import java.util.*

object Tools {
    fun dpToPx(dp: Int): Float {
        return (dp * Resources.getSystem().displayMetrics.density)
    }

    fun isEmailValid(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    fun passwordValidation(password: String) = (password.length > 5)

    fun initDialog(context: Context, title:String, description:String) {
        val dialog = Dialog(context)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.error_dialog_layout)

        val params: ViewGroup.LayoutParams = dialog.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams
        dialog.errorDialogTitleTV.text = title
        dialog.errorDialogDescriptionTV.text = description
        dialog.errorDialogButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun simpleDateFormatToString(date:String) : String {
        val split = date.split("-")
        if (split.size ==3){
            val year = split[0]
            var month = split[1]
            val day = split[2]
            when (month) {
                "01" -> month = "January"
                "02" -> month = "February"
                "03" -> month = "March"
                "04" -> month = "April"
                "05" -> month = "May"
                "06" -> month = "June"
                "07" -> month = "July"
                "08" -> month = "August"
                "09" -> month = "September"
                "10" -> month = "October"
                "11" -> month = "November"
                "12" -> month = "December"
            }
            return "$day $month $year"
        } else {
            return ""
        }

    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentDates():Map<String,String>{
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val cal = Calendar.getInstance()
        Log.d("CurrentDate: ", sdf.format(cal.time))
        cal.add(Calendar.DAY_OF_MONTH, -10)

        val from = sdf.format(cal.time)
        cal.add(Calendar.DAY_OF_MONTH, 25)


        val to = sdf.format(cal.time)

       return mapOf<String,String>("from" to from,"to" to to)
    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentTime():String{
        val simpleDateFormat = SimpleDateFormat("d MMM hh:mm aaa")
        val date = Date()
        return  simpleDateFormat.format(date)
    }



    fun isInternetAvailable(): Boolean {
        var result = false
        val connectivityManager =
            App.context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }
        return result
    }




}

