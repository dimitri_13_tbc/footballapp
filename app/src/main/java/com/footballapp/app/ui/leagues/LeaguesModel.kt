package com.footballapp.app.ui.leagues

import com.google.gson.annotations.SerializedName

class LeaguesModel {
    @SerializedName("country_id")
    val countryId = ""

    @SerializedName("country_name")
    val countryName = ""

    @SerializedName("league_id")
    val leagueId = ""

    @SerializedName("league_name")
    val leagueName = ""

    @SerializedName("league_season")
    val leagueSeason = ""

    @SerializedName("league_logo")
    val leagueLogo = ""

    @SerializedName("country_logo")
    val countryLogo = ""

}