package com.footballapp.app.ui.main.account

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.footballapp.app.R
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.tools.extensions.visible
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.clubs.ClubModel
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.account.login.LogInActivity
import com.footballapp.app.ui.main.account.login.LogInViewModel
import com.footballapp.app.ui.main.account.sign_up.SignUpActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_account.view.*
import kotlinx.android.synthetic.main.main_toolbar_layout.view.*

class AccountFragment(private val clubModel: ClubModel) : BaseFragment() {

    private lateinit var viewModel: LogInViewModel
    private val fb = FirebaseAuth.getInstance()
    override fun layoutResource() = R.layout.fragment_account

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {

        viewModel = ViewModelProvider(requireActivity())[LogInViewModel::class.java]
        itemView!!.toolbarTV.text = getString(R.string.profile)

        if (fb.uid != null)
            userLoggedIn()
        else
            userNotLoggedIn()

        setClickListeners()
    }

    private fun setClickListeners() {
        itemView!!.signOutButton.setOnClickListener {
            fb.signOut()
            val intent = Intent(context,HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            HomeActivity.userActive = true
            userNotLoggedIn()
        }
        itemView!!.registerButton.setOnClickListener {
            val intent = Intent(context, SignUpActivity::class.java)
            startActivity(intent)
        }
        itemView!!.authButton.setOnClickListener {
            val intent = Intent(context, LogInActivity::class.java)
            startActivity(intent)
        }
    }


    private fun userLoggedIn() {
        getUserProfile()
        itemView!!.profileFavoriteClubImageView.setGlideImage(clubModel.teamBadge)
        itemView!!.loginContainer.visible(false)
        itemView!!.profileInfoLayout.visible(true)
        itemView!!.accountToolbar.visible(true)
    }

    private fun userNotLoggedIn() {
        itemView!!.loginContainer.visible(true)
        itemView!!.profileInfoLayout.visible(false)
        itemView!!.accountToolbar.visible(false)
    }


    private fun getUserProfile() {
        val uid = FirebaseAuth.getInstance().uid ?: ""

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("/users/$uid")

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(UserModel::class.java)

                itemView!!.profileUserNameTextView.text = value!!.username
                itemView!!.profileEmailTextView.text = value.email
                itemView!!.profileImageView.setGlideImage(value.profileImageUrl)

            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("onDataChange", "Failed to read value.", error.toException())
            }
        })
    }
}