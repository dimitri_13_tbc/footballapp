package com.footballapp.app.ui.main.competitions.tab_layouts.schedule.next_matchday

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.R
import com.footballapp.app.callbacks.MatchRecyclerViewInterface
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.models.MatchModel
import com.footballapp.app.ui.main.adapters.ScheduleMatchesRecyclerViewAdapter
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.events.EventContainerFragment
import com.footballapp.app.ui.main.events.ScheduledMatchesPreviewFragment
import kotlinx.android.synthetic.main.activity_bottom_navigation.*
import kotlinx.android.synthetic.main.fragment_next_match_day.view.*

class NextMatchDayFragment(private val nextMatch: MutableList<MatchModel>) : BaseFragment() , MatchRecyclerViewInterface {
    override fun layoutResource() = R.layout.fragment_next_match_day
    lateinit var adapter: ScheduleMatchesRecyclerViewAdapter
    private lateinit var homeActivity: HomeActivity
    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }

    private fun init() {
        homeActivity = activity as HomeActivity
        adapter =
            ScheduleMatchesRecyclerViewAdapter(
                nextMatch, this
            )
        itemView!!.nextMatchRV.layoutManager = LinearLayoutManager(context)
        itemView!!.nextMatchRV.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onMatchClick(matchModel: MatchModel) {
        Log.d("clickeddd3", matchModel.matchStatus)
        if (matchModel.matchStatus.contains("Finished")) {
            val beginTransaction = homeActivity.supportFragmentManager.beginTransaction()
            beginTransaction.add(
                R.id.fragmentContainer,
                EventContainerFragment(
                    matchModel
                )
            )
            beginTransaction.addToBackStack("EVENT")
            beginTransaction.commit()
            homeActivity.fragmentContainer.visibility = View.VISIBLE
            homeActivity.viewPager.visibility = View.INVISIBLE
        }else{
            val beginTransaction = homeActivity.supportFragmentManager.beginTransaction()
            beginTransaction.add(
                R.id.fragmentContainer,
                ScheduledMatchesPreviewFragment(
                    matchModel
                )
            )
            beginTransaction.addToBackStack("EVENT")
            beginTransaction.commit()
            homeActivity.fragmentContainer.visibility = View.VISIBLE
            homeActivity.viewPager.visibility = View.INVISIBLE
        }
    }

}




