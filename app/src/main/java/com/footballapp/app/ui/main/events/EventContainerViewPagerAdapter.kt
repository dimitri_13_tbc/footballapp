package com.footballapp.app.ui.main.events

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class EventContainerViewPagerAdapter(private val items:List<Fragment>,fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int) = items[position]

    override fun getCount() = items.size

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Stats"
            1 -> "Lineup"
            else -> ""
        }
    }
}