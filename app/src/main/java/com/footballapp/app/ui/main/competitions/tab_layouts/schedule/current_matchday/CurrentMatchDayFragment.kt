package com.footballapp.app.ui.main.competitions.tab_layouts.schedule.current_matchday

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.R
import com.footballapp.app.callbacks.MatchRecyclerViewInterface
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.models.MatchModel
import com.footballapp.app.ui.main.adapters.ScheduleMatchesRecyclerViewAdapter
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.events.EventContainerFragment
import com.footballapp.app.ui.main.events.ScheduledMatchesPreviewFragment
import kotlinx.android.synthetic.main.activity_bottom_navigation.*
import kotlinx.android.synthetic.main.fragment_current_match_day.view.*

class CurrentMatchDayFragment(private val currentMatches:MutableList<MatchModel>) : BaseFragment() , MatchRecyclerViewInterface {
    override fun layoutResource() = R.layout.fragment_current_match_day
    private lateinit var homeActivity: HomeActivity
    lateinit var adapter: ScheduleMatchesRecyclerViewAdapter

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }
    private fun init() {
        homeActivity = activity as HomeActivity
        adapter  =
            ScheduleMatchesRecyclerViewAdapter(
                currentMatches, this
            )
        itemView!!.currentMatchRV.layoutManager = LinearLayoutManager(context)
        itemView!!.currentMatchRV.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onMatchClick(matchModel: MatchModel) {
        Log.d("clickeddd2", matchModel.matchStatus)
        if (matchModel.matchStatus.contains("Finished")) {
            val beginTransaction = homeActivity.supportFragmentManager.beginTransaction()
            beginTransaction.add(
                R.id.fragmentContainer,
                EventContainerFragment(
                    matchModel
                )
            )
            beginTransaction.addToBackStack("EVENT")
            beginTransaction.commit()
            homeActivity.fragmentContainer.visibility = View.VISIBLE
            homeActivity.viewPager.visibility = View.INVISIBLE
        }else{
            val beginTransaction = homeActivity.supportFragmentManager.beginTransaction()
            beginTransaction.add(
                R.id.fragmentContainer,
                ScheduledMatchesPreviewFragment(
                    matchModel
                )
            )
            beginTransaction.addToBackStack("EVENT")
            beginTransaction.commit()
            homeActivity.fragmentContainer.visibility = View.VISIBLE
            homeActivity.viewPager.visibility = View.INVISIBLE
        }

    }

}