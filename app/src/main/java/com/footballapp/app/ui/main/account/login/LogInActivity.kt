package com.footballapp.app.ui.main.account.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.footballapp.app.R
import com.footballapp.app.databinding.ActivityLogInBinding
import com.footballapp.app.tools.Tools
import com.footballapp.app.tools.extensions.setValidColor
import com.footballapp.app.tools.extensions.visible
import com.footballapp.app.ui.main.HomeActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.progressbar_layout.*

class LogInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var viewModel: LogInViewModel
    private lateinit var binding: ActivityLogInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_log_in)
        viewModel = ViewModelProvider(this)[LogInViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        auth = FirebaseAuth.getInstance()
        init()
    }

    private fun init() {
        setObservers()
        logInButton.setOnClickListener {
            logIn()
        }
    }

    private fun logIn() {
        if (Tools.isEmailValid(viewModel.emailLiveData.value!!) && Tools.passwordValidation(viewModel.passwordLiveData.value!!)){
            spinKitContainer.visible(true)
            auth.signInWithEmailAndPassword(viewModel.emailLiveData.value!!, viewModel.passwordLiveData.value!!)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, HomeActivity::class.java)
                        HomeActivity.userActive = true
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        spinKitContainer.visible(false)
                        startActivity(intent)

                    } else {
                        spinKitContainer.visible(false)
                        Toast.makeText(
                            baseContext, "password or email is incorrect",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        } else {
            Tools.initDialog(this,getString(R.string.email_or_password_not_valid),getString(R.string.please_enter_valid))
        }

    }

    private fun setObservers(){
        viewModel.emailLiveData.observe(this, Observer {
            emailLogInEditText.setValidColor(Tools.isEmailValid(it))
        })
        viewModel.passwordLiveData.observe(this, Observer {
            passwordLogInEditText.setValidColor(Tools.passwordValidation(it))
        })
    }


}