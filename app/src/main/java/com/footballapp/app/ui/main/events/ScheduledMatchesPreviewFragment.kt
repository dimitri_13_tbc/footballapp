package com.footballapp.app.ui.main.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.FragmentManager
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.HomeActivity
import kotlinx.android.synthetic.main.activity_bottom_navigation.*
import kotlinx.android.synthetic.main.choose_team_toolbar_layout.view.*
import kotlinx.android.synthetic.main.choose_team_toolbar_layout.view.toolbarTV
import kotlinx.android.synthetic.main.fragment_event_container.view.*
import kotlinx.android.synthetic.main.fragment_scheduled_matches_preview.*
import kotlinx.android.synthetic.main.fragment_scheduled_matches_preview.view.*
import kotlinx.android.synthetic.main.main_toolbar_layout.view.*

class ScheduledMatchesPreviewFragment(private val matchModel: MatchModel) : BaseFragment() {
    private lateinit var homeActivity: HomeActivity
    override fun layoutResource() = R.layout.fragment_scheduled_matches_preview

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        homeActivity = activity as HomeActivity
        init()
        backClick()
    }

    private fun init() {
        itemView!!.toolbarTV.text = SharedPreference.instance().getString(SharedPreference.LEAGUE_NAME)
        itemView!!.toolbarImageView.setGlideImage(SharedPreference.instance().getString(SharedPreference.LEAGUE_LOGO)!!)
        itemView!!.homeTeamImageView.setGlideImage(matchModel.teamHomeBadge)
        itemView!!.stadiumTV.text = matchModel.matchStadium
        itemView!!.dateTV.text = matchModel.getDateWithWord()
        itemView!!.timeTV.text = matchModel.matchTime
        itemView!!.awayTeamImageView.setGlideImage(matchModel.teamAwayBadge)
    }
    private fun backClick() {
        homeActivity.onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    homeActivity.viewPager.visibility = View.VISIBLE
                    homeActivity.fragmentContainer.visibility = View.GONE
                    homeActivity.supportFragmentManager.popBackStack(
                        "EVENT",
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )

                }

            })
    }
}