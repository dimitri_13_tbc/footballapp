package com.footballapp.app.ui.main.chat

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.R
import com.footballapp.app.tools.Tools
import com.footballapp.app.tools.extensions.visible
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.clubs.ClubModel
import com.footballapp.app.ui.main.account.UserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_chat.view.*
import java.util.*

class ChatFragment(private var clubModel: ClubModel) : BaseFragment() {
    override fun layoutResource() = R.layout.fragment_chat

    lateinit var adapter: ChatRecyclerViewAdapter
    private val fb = FirebaseAuth.getInstance()
    private val messages = mutableListOf<ChatModel>()
    private var selectedPhotoUri: Uri? = null

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }

    private fun init() {
        adapter = ChatRecyclerViewAdapter(messages)
        itemView!!.chatRecyclerView.layoutManager = LinearLayoutManager(context)
        itemView!!.chatRecyclerView.adapter = adapter
        if (fb.uid != null) {
            getMessages()
            itemView!!.chatSendMsgImageView.isClickable = true
        }else{
            itemView!!.chatSendMsgImageView.isClickable = false
        }

        itemView!!.sendMessageButton.setOnClickListener {
            if (fb.uid != null) {
                itemView!!.sendMessageButton.isClickable = false
                if (selectedPhotoUri != null) {
                    uploadImageToFirebaseStorage()
                } else if (itemView!!.messageEditText.text.isNotEmpty() && selectedPhotoUri == null) {
                    itemView!!.chatSendMsgImageView.setImageBitmap(null)
                    sendMessage("")
                }
            } else
                Tools.initDialog(
                    context!!,
                    "Chat is disabled",
                    "You must be authorized in order to use the chat"
                )
        }
        itemView!!.chooseChatImageButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/+"
            startActivityForResult(intent, 0)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            selectedPhotoUri = data.data


            val bitmap =
                MediaStore.Images.Media.getBitmap(context!!.contentResolver, selectedPhotoUri)
            itemView!!.chatSendMsgImageView.visible(true)
            itemView!!.chatSendMsgImageView.setImageBitmap(bitmap)

        }

    }

    private fun uploadImageToFirebaseStorage() {

        if (selectedPhotoUri == null){
            itemView!!.chatSendMsgImageView.setImageBitmap(null)
            return
        }

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {

                ref.downloadUrl.addOnSuccessListener {

                    sendMessage(it.toString())
                }
            }
            .addOnFailureListener {
                itemView!!.sendMessageButton.isClickable = true
            }
    }

    private fun sendMessage(imageMessage: String) {
        val userId = FirebaseAuth.getInstance().uid ?: ""
        val messageReference = FirebaseDatabase.getInstance().getReference("/messages").push()
        val chatModel = ChatModel()
        val userReference = FirebaseDatabase.getInstance().getReference("/users/$userId")
        userReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(UserModel::class.java)
                chatModel.id = userId
                if (itemView!!.messageEditText.text.isNotEmpty()) {
                    chatModel.text = itemView!!.messageEditText.text.toString()
                }
                chatModel.username = value!!.username
                chatModel.profileImgUrl = value.profileImageUrl
                chatModel.time = Tools.getCurrentTime()
                chatModel.favouriteClubUrl = clubModel.teamBadge
                if (imageMessage.isNotEmpty()) {
                    chatModel.imageMsg = imageMessage
                }
                messageReference.setValue(chatModel)
                    .addOnSuccessListener {
                        itemView!!.messageEditText.text.clear()
                        itemView!!.chatSendMsgImageView.setImageBitmap(null)
                        itemView!!.chatSendMsgImageView.visible(false)
                        selectedPhotoUri = null
                        itemView!!.sendMessageButton.isClickable = true
                    }
                    .addOnFailureListener {
                        itemView!!.sendMessageButton.isClickable = true
                    }
            }

            override fun onCancelled(error: DatabaseError) {
                itemView!!.sendMessageButton.isClickable = true
            }
        })

    }

    private fun getMessages() {
        val ref = FirebaseDatabase.getInstance().getReference("messages")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val chatMessage = snapshot.getValue(ChatModel::class.java)!!

                messages.add(chatMessage)
                adapter.notifyItemInserted(messages.size - 1)
                itemView!!.chatRecyclerView.scrollToPosition(adapter.itemCount - 1)
            }

            override fun onCancelled(error: DatabaseError) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

        })

    }

}