package com.footballapp.app.services.shared_pref

import android.content.Context
import android.content.SharedPreferences
import com.footballapp.app.App

class SharedPreference {
    companion object {
        const val CLUB = "CLUB"
        const val LEAGUE = "LEAGUE"
        const val LEAGUE_LOGO = "LEAGUE_LOGO"
        const val LEAGUE_NAME = "LEAGUE_NAME"
        const val FOOTBALL_API_KEY = "FOOTBALL_API_KEY"

        private var sharedPreference: SharedPreference? = null
        fun instance(): SharedPreference {
            if (sharedPreference == null) {
                sharedPreference =
                    SharedPreference()
            }
            return sharedPreference!!
        }
    }


    private val sharedPreferences: SharedPreferences by lazy {
        App.context!!.getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
    }

    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }

    fun saveString(key:String,value:String){
        editor.putString(key,value)
        editor.apply()
    }

    fun getString(key: String) = sharedPreferences.getString(key,"")

    fun clear(): SharedPreferences.Editor = editor.clear()

    fun delete(key: String){
        if (sharedPreferences.contains(key)){
            editor.remove(key)
        }
        editor.apply()
    }
}