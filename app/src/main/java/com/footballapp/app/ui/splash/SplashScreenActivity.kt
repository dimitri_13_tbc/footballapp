package com.footballapp.app.ui.splash


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.Log.d
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.footballapp.app.R
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.Endpoints
import com.footballapp.app.services.network.FootballDataLoader
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.tools.Tools
import com.footballapp.app.tools.extensions.visible
import com.footballapp.app.ui.clubs.ClubModel
import com.footballapp.app.ui.countries.CountriesActivity
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.account.UserModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_splash_screen.*
import kotlinx.android.synthetic.main.progressbar_layout.*


class SplashScreenActivity : AppCompatActivity() {

    private lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        handler = Handler()
        splashLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in))
        splashTextView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up))

    }

    override fun onResume() {
        super.onResume()
        getApiKey()
        handler.postDelayed(runnable, 3000)

    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

    private val runnable = Runnable {
        init()
    }

    private fun init() {
        val activity: AppCompatActivity
        if (Tools.isInternetAvailable()) {
            if (SharedPreference.instance().getString(SharedPreference.CLUB).isNullOrEmpty()
            ) {
                activity = CountriesActivity()
                toActivity(activity)
            } else {
                activity = HomeActivity()
                getClub(activity)
            }
        } else {

            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_LONG)
                .show()
            tryAgainButton.visible(true)
            tryAgainButton.setOnClickListener {
                init()
            }

        }


    }

    private fun toActivity(activity: AppCompatActivity) {
        val intent = Intent(this, activity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
    }

    private fun getApiKey() {
        d("keyy", "dsadas")
        val database = FirebaseDatabase.getInstance()
        val myRef = database.reference

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(GetApiKeyModel::class.java)
                d("keyy", value!!.footballApiKey)
                d("keyy", "value as String")
                SharedPreference.instance()
                    .saveString(SharedPreference.FOOTBALL_API_KEY, value!!.footballApiKey)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("onDataChange", "Failed to read value.", error.toException())
            }
        })
    }

    private fun getClub(activity: AppCompatActivity) {

        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_TEAMS
        parameters["team_id"] = SharedPreference.instance().getString(SharedPreference.CLUB)!!
        parameters["APIkey"] = SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        d("keyy2",SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!)
        d("keyy2",SharedPreference.instance().getString(SharedPreference.CLUB)!!)
        FootballDataLoader.getRequest(spinKitContainer, parameters, object : CustomCallback {
            override fun onResponse(response: String) {
                d("rsponsee",response)
                val club = Gson().fromJson(response, Array<ClubModel>::class.java).toList()
                HomeActivity.clubModel = club[0]
                toActivity(activity)
            }
        })

    }

}