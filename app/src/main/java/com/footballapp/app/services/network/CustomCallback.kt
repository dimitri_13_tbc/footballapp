package com.footballapp.app.services.network

interface CustomCallback {
    fun onResponse(response:String){}
    fun onFailure(title:String,errorMessage:String){}
}