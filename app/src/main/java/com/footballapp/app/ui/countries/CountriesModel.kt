package com.footballapp.app.ui.countries

import com.google.gson.annotations.SerializedName

class CountriesModel {
    @SerializedName("country_id")
    val countryId:String = ""
    @SerializedName("country_name")
    val countryName:String = ""
    @SerializedName("country_logo")
    val countryLogo:String = ""

}