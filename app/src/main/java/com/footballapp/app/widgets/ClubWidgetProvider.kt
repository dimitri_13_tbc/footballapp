package com.footballapp.app.widgets

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.util.Log.d
import android.widget.RemoteViews
import com.bumptech.glide.Glide
import com.footballapp.app.App
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.Endpoints
import com.footballapp.app.services.network.FootballDataLoader
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.tools.Tools
import com.footballapp.app.ui.main.HomeActivity
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject


class ClubWidgetProvider : AppWidgetProvider() {


    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)

        getLastAndNextMatch()
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val componentName =
            ComponentName(context!!.packageName, javaClass.name)
        val appWidgetIds = appWidgetManager.getAppWidgetIds(componentName)
        onUpdate(context, appWidgetManager, appWidgetIds)
    }

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray?
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)


        getLastAndNextMatch()
        val n = appWidgetIds!!.size
        for (i in 0 until n) {
            val appWidgetId = appWidgetIds[i]
            val intent =
                Intent(context, ClubWidgetProvider::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
            val lastMatchIntent = Intent(context,HomeActivity::class.java)
            val widgetClick = PendingIntent.getActivity(context,1,lastMatchIntent,0)

            val views =
                RemoteViews(context!!.packageName, R.layout.club_widget_layout)

            views.setOnClickPendingIntent(R.id.widgetRefreshButton, pendingIntent)
            views.setOnClickPendingIntent(R.id.widgetLastMatchContainer,widgetClick)
            views.setTextViewText(R.id.widgetLastMatchScore,HomeActivity.lastMatchModel.getFullTimeScore())
            views.setTextViewText(R.id.widgetLAstMatchDate,HomeActivity.lastMatchModel.getDateWithWord())
            views.setTextViewText(R.id.widgetNextMatchScore,HomeActivity.nextMatchModel.matchTime)
            views.setTextViewText(R.id.widgetNextMatchDate,HomeActivity.nextMatchModel.getDateWithWord())
            views.setImageViewBitmap(R.id.widgetLastMatchHomeTeamImageView,HomeActivity.lastMatchHomeTeamBitmap)
            views.setImageViewBitmap(R.id.widgetLastMatchAwayTeamImageView,HomeActivity.lastMatchAwayTeamBitmap)
            views.setImageViewBitmap(R.id.widgetNextMatchHomeTeamImageView,HomeActivity.nextMatchHomeTeamBitmap)
            views.setImageViewBitmap(R.id.widgetNextMatchAwayTeamImageView,HomeActivity.nextMatchAwayTeamBitmap)


            appWidgetManager!!.updateAppWidget(appWidgetId, views)
        }

    }
    private fun getLastAndNextMatch() {
        val currentDates = Tools.getCurrentDates()

        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_EVENTS
        parameters["from"] = currentDates.getValue("from")
        parameters["to"] = currentDates.getValue("to")
        parameters["team_id"] = SharedPreference.instance().getString(SharedPreference.CLUB)!!
        parameters["APIkey"] = SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        FootballDataLoader.getRequest(null, parameters, object : CustomCallback {
            override fun onResponse(response: String) {
                if (response[0] == '['){
                    val list = Gson().fromJson(response, Array<MatchModel>::class.java).toList()
                    for (i in list.indices) {
                        if (list[i].matchStatus.isEmpty()) {
                            HomeActivity.nextMatchModel = list[i]
                            nextMatch(HomeActivity.nextMatchModel)
                            HomeActivity.lastMatchModel = list[i-1]
                            lastMatch(HomeActivity.lastMatchModel)
                            break
                        }
                    }
                }


            }
        })
    }
    private fun lastMatch(matchModel: MatchModel) {


        CoroutineScope(IO).launch {
            val get1 = Glide.with(App.context!!)
                .asBitmap()
                .load(matchModel.teamHomeBadge)
                .submit()
                .get()
            val get = Glide.with(App.context!!)
                .asBitmap()
                .load(matchModel.teamAwayBadge)
                .submit()
                .get()
            CoroutineScope(Main).launch {
                HomeActivity.lastMatchAwayTeamBitmap = get
                HomeActivity.lastMatchHomeTeamBitmap = get1
            }
        }

    }

    private fun nextMatch(matchModel: MatchModel) {

        CoroutineScope(IO).launch {
            val get1 = Glide.with(App.context!!)
                .asBitmap()
                .load(matchModel.teamHomeBadge)
                .submit()
                .get()
            val get = Glide.with(App.context!!)
                .asBitmap()
                .load(matchModel.teamAwayBadge)
                .submit()
                .get()
            CoroutineScope(Main).launch {
                HomeActivity.nextMatchAwayTeamBitmap = get
                HomeActivity.nextMatchHomeTeamBitmap = get1
            }
        }

    }




}





