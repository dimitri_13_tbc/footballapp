package com.footballapp.app.ui.main.competitions.tab_layouts.standings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import kotlinx.android.synthetic.main.standing_table_recyclerview_item_layout.view.*

class TableStandingRecyclerViewAdapter(private val items: MutableList<StandingModel>) :
    RecyclerView.Adapter<TableStandingRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.standing_table_recyclerview_item_layout, parent, false
        )
    )


    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: StandingModel
        fun onBind() {
            model = items[adapterPosition]
            if (model.overallLeaguePosition.toInt() % 2 == 1) {
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.colorPrimaryDark
                    )
                )
            } else {
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.standingItemBackgroundColor
                    ))
            }
            itemView.pointTextView.text = model.overallLeaguePTS
            itemView.drawTextView.text = model.overallLeagueD
            itemView.gamePlayedTextView.text = model.overallLeaguePlayer
            itemView.goalAgainstTextView.text = model.overallLeagueGA
            itemView.goalForwardTextView.text = model.overallLeagueGF
            itemView.loseTextView.text = model.overallLeagueL
            itemView.winTextView.text = model.overallLeagueW
        }
    }
}