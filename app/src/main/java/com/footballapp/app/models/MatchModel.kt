package com.footballapp.app.models

import com.footballapp.app.tools.Tools
import com.google.gson.annotations.SerializedName

class MatchModel {

    @SerializedName("match_id")
    var matchId: String? = ""

    @SerializedName("country_id")
    val countryId = ""

    @SerializedName("country_name")
    val countryName = ""

    @SerializedName("league_id")
    val leagueId = ""

    @SerializedName("league_name")
    val leagueName = ""

    @SerializedName("match_date")
    val matchDate = ""

    @SerializedName("match_status")
    val matchStatus: String = ""

    @SerializedName("match_time")
    val matchTime = ""

    @SerializedName("match_hometeam_id")
    val matchHomeTeamId = ""

    @SerializedName("match_hometeam_name")
    val matchHomeTeamName = ""

    @SerializedName("match_hometeam_score")
    val matchHomeTeamScore = ""

    @SerializedName("match_awayteam_name")
    val matchAwayTeamName = ""

    @SerializedName("match_awayteam_id")
    val matchAwayTeamId = ""

    @SerializedName("match_awayteam_score")
    val matchAwayTeamScore = ""

    @SerializedName("match_hometeam_halftime_score")
    val matchHomeTeamHalftimeScore = ""

    @SerializedName("match_awayteam_halftime_score")
    val matchAwayTeamHalftimeScore = ""

    @SerializedName("match_hometeam_extra_score")
    val matchHomeTeamExtraScore = ""

    @SerializedName("match_awayteam_extra_score")
    val matchAwayTeamExtraScore = ""

    @SerializedName("match_hometeam_penalty_score")
    val matchHomeTeamPenaltyScore = ""

    @SerializedName("match_awayteam_penalty_score")
    val matchAwayTeamPenaltyScore = ""

    @SerializedName("match_hometeam_ft_score")
    val matchHomeTeamFtScore = ""

    @SerializedName("match_awayteam_ft_score")
    val matchAwayTeamFtScore = ""

    @SerializedName("match_hometeam_system")
    val matchHomeTeamSystem = ""

    @SerializedName("match_awayteam_system")
    val matchAwayTeamSystem = ""

    @SerializedName("match_live")
    val matchLive = ""

    @SerializedName("match_round")
    val matchRound = ""

    @SerializedName("match_stadium")
    val matchStadium = ""

    @SerializedName("match_referee")
    val matchReferee = ""

    @SerializedName("team_home_badge")
    val teamHomeBadge = ""

    @SerializedName("team_away_badge")
    val teamAwayBadge = ""

    @SerializedName("league_logo")
    val leagueLogo = ""

    val goalscorer = mutableListOf<Goalscorer>()
    val cards = mutableListOf<Cards>()
    val substitutions = Substitutions()
    val lineup = Lineup()
    val statistics = mutableListOf<Statistics>()

    class Goalscorer {
        val time = ""

        @SerializedName("home_scorer")
        val homeScorer = ""
        val score = ""

        @SerializedName("away_scorer")
        val awayScorer = ""
        val info = ""

        fun getGoalTime():String{
            return "$time'"
        }

    }

    class Cards {
        val time = ""

        @SerializedName("home_fault")
        val homeFault = ""
        val card = ""

        @SerializedName("away_fault")
        val awayFault = ""
        val info = ""

        fun getCardTime():String{
            return "$time'"
        }
    }

    class Substitutions {
        val home = mutableListOf<Home>()
        val away = mutableListOf<Away>()

        class Home {
            val time = ""
            val substitution = ""
        }

        class Away {
            val time = ""
            val substitution = ""
        }
    }

    class Lineup {
        val home = Home()
        val away = Away()

        class Home {
            @SerializedName("starting_lineups")
            val startingLineups = mutableListOf<StartingLineups>()
            val substitutes = mutableListOf<Substitutes>()
            val coach = mutableListOf<Coach>()

            @SerializedName("missing_players")
            val missingPlayers = mutableListOf<MissingPlayers>()

            class StartingLineups {
                @SerializedName("lineup_player")
                val lineupPlayer = ""

                @SerializedName("lineup_number")
                val lineupNumber = ""

                @SerializedName("lineup_position")
                val lineupPosition = ""
            }


            class Substitutes {
                @SerializedName("lineup_player")
                val lineupPlayer = ""

                @SerializedName("lineup_number")
                val lineupNumber = ""

                @SerializedName("lineup_position")
                val lineupPosition = ""
            }


            class Coach {
                @SerializedName("lineup_player")
                val lineupPlayer = ""

                @SerializedName("lineup_number")
                val lineupNumber = ""

                @SerializedName("lineup_position")
                val lineupPosition = ""
            }

            class MissingPlayers {
                @SerializedName("lineup_player")
                val lineupPlayers = ""

                @SerializedName("lineup_number")
                val lineupNumber = ""

                @SerializedName("lineup_position")
                val lineupPosition = ""
            }
        }

        class Away {
            @SerializedName("starting_lineups")
            val startingLineups = mutableListOf<StartingLineups>()


            inner class StartingLineups {
                @SerializedName("lineup_player")
                val lineupPlayer = ""

                @SerializedName("lineup_number")
                val lineupNumber = ""

                @SerializedName("lineup_position")
                val lineupPosition = ""
            }

            val substitutes = mutableListOf<Substitutes>()

            inner class Substitutes {
                @SerializedName("lineup_player")
                val lineupPlayer = ""

                @SerializedName("lineup_number")
                val lineupNumber = ""

                @SerializedName("lineup_position")
                val lineupPosition = ""
            }

            val coach = mutableListOf<Coach>()

            inner class Coach {
                @SerializedName("lineup_player")
                val lineupPlayer = ""

                @SerializedName("lineup_number")
                val lineupNumber = ""

                @SerializedName("lineup_position")
                val lineupPosition = ""
            }

            @SerializedName("missing_players")
            val missingPlayers = mutableListOf<MissingPlayers>()

            inner class MissingPlayers {
                @SerializedName("lineup_player")
                val lineupPlayers = ""

                @SerializedName("lineup_number")
                val lineupNumber = ""

                @SerializedName("lineup_position")
                val lineupPosition = ""
            }
        }
    }


    class Statistics {
        val type = ""
        val home = ""
        val away = ""
    }

    fun getFullTimeScore(): String {
        return if (matchHomeTeamScore.isNotEmpty() && matchAwayTeamScore.isNotEmpty())
            "$matchHomeTeamScore : $matchAwayTeamScore"
        else
            ""
    }

    fun getDateWithWord(): String {
        return Tools.simpleDateFormatToString(matchDate)
    }




}