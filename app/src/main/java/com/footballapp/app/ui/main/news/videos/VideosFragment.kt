package com.footballapp.app.ui.main.news.videos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.footballapp.app.R
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.VideosDataLoader
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.news.news_recycler_adapters.OnLoadMoreListener
import org.json.JSONObject
import kotlinx.android.synthetic.main.fragment_videos.view.*


class VideosFragment : BaseFragment() {
    private lateinit var videosRecyclerViewAdapter: VideosRecyclerViewAdapter
    private val videos = mutableListOf<VideoModel>()

    var nextPageToken = ""


    override fun layoutResource() = R.layout.fragment_videos

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }

    private fun init() {
        itemView!!.videosRecyclerView.layoutManager = LinearLayoutManager(context)

        videosRecyclerViewAdapter =
            VideosRecyclerViewAdapter(videos, itemView!!.videosRecyclerView, loadMoreListener)
        itemView!!.videosRecyclerView.adapter = videosRecyclerViewAdapter

        itemView!!.videosSwipeRefresh.setOnRefreshListener {
            nextPageToken = ""
            videos.clear()
            getVideosID(null)
        }


        getVideosID(null)
    }


    private fun getVideosID(nextPage: String?) {
        val parameters = mutableMapOf<String, String>()
        parameters["key"] = context!!.getString(R.string.VIDEOS_API_KEY)
        parameters["part"] = "snippet"
        if (nextPage != null)
            parameters["pageToken"] = nextPage
        parameters["q"] = HomeActivity.clubModel.teamName
        VideosDataLoader.getRequest(null, parameters, object : CustomCallback {
            override fun onResponse(response: String) {

                val lastPosition = videos.size
                if (lastPosition > 0) {
                    videos.removeAt(videos.size - 1)
                    videosRecyclerViewAdapter.notifyItemRemoved(videos.size - 1)
                }
                videosRecyclerViewAdapter.setLoaded()

                val json = JSONObject(response)
                nextPageToken = json.getString("nextPageToken")

                if (json.has("items")) {
                    val jsonArray = json.getJSONArray("items")
                    for (i in 0 until jsonArray.length()) {
                        val videoModel = VideoModel()
                        val obj = jsonArray[i] as JSONObject
                        if (obj.has("id") && obj.has("snippet")) {
                            val idObj = obj.getJSONObject("id")
                            val snippet = obj.getJSONObject("snippet")
                            if (idObj.has("videoId") && snippet.has("title")) {
                                val videoId = idObj.getString("videoId")
                                val title = snippet.getString("title")
                                videoModel.videoName = title
                                videoModel.videoId = videoId
                                videos.add(videoModel)
                            }


                        }


                    }
                }



                videosRecyclerViewAdapter.notifyDataSetChanged()
                itemView!!.videosSwipeRefresh.isRefreshing = false

            }
        })
    }


    private val loadMoreListener = object :
        OnLoadMoreListener {
        override fun onLoadMore() {
            if (videos.size != 0) {
                if (!videos[videos.size - 1].isLast) {
                    itemView!!.videosRecyclerView.post {

                        val videoModel =
                            VideoModel()
                        videoModel.isLast = true
                        videos.add(videoModel)
                        videosRecyclerViewAdapter.notifyItemInserted(videos.size - 1)
                        getVideosID(nextPageToken)


                    }
                }
            }
        }
    }
}

