package com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.substitutes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.event_home_player_info_recyclerview_item_layout.view.*

class HomeTeamSubstitutesRecyclerViewAdapter(private val items: List<MatchModel.Lineup.Home.Substitutes>) :
    RecyclerView.Adapter<HomeTeamSubstitutesRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_home_player_info_recyclerview_item_layout, parent, false)
        )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: MatchModel.Lineup.Home.Substitutes
        fun onBind() {
            model = items[adapterPosition]
            itemView.homeTeamNumberShirtButton.text = model.lineupNumber
            itemView.homeTeamNameTextView.text = model.lineupPlayer
            itemView.homeTeamScoreTimeTextView.visible(false)
            if (adapterPosition % 2 == 0) {
                itemView.homePlayersContainer.setBackgroundResource(R.color.transparentGray)
            } else {
                itemView.homePlayersContainer.setBackgroundResource(android.R.color.transparent)
            }
        }

    }
}