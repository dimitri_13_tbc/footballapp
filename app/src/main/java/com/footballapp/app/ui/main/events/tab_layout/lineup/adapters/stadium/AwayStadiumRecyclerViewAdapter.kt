package com.footballapp.app.ui.main.events.tab_layout.lineup.adapters.stadium

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.App.Companion.context
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import kotlinx.android.synthetic.main.lineup_stadium_rv_layout.view.*

class AwayStadiumRecyclerViewAdapter(
    private val awayPlayers: MutableList<MatchModel.Lineup.Away.StartingLineups>,
    private val cards: MutableList<MatchModel.Cards>
) :
    RecyclerView.Adapter<AwayStadiumRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.lineup_stadium_rv_layout, parent, false
        )
    )

    override fun getItemCount() = awayPlayers.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var awayModel: MatchModel.Lineup.Away.StartingLineups
        fun onBind() {
            awayModel = awayPlayers[adapterPosition]
            itemView.lineupPlayersTV.text = awayModel.lineupPlayer
            itemView.lineupPlayersButton.text = awayModel.lineupNumber
            itemView.lineupPlayersButton.setBackgroundResource(R.mipmap.shirt_2)
            itemView.lineupPlayersButton.setTextColor(ContextCompat.getColor(context!!,R.color.colorPrimaryDark))
            cards.forEach {
                if (it.card.contains("yellow card") && it.awayFault.isNotEmpty()
                    && it.awayFault.contains(awayModel.lineupPlayer)
                ) {
                    itemView.lineupFoulCardImageView.setImageResource(R.mipmap.ic_card_yellow)
                } else if (it.card.contains("red card") && it.awayFault.isNotEmpty() &&
                    it.awayFault.contains(awayModel.lineupPlayer)
                ) {
                    itemView.lineupFoulCardImageView.setImageResource(R.mipmap.ic_card_red)
                }
            }

        }
    }
}