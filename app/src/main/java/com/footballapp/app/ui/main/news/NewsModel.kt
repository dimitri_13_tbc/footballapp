package com.footballapp.app.ui.main.news

import com.footballapp.app.tools.Tools

class NewsModel {
    var status = ""
    var totalResults = 0
    val articles = mutableListOf<Articles>()

    class Articles {
        class Source{
            val id:String? = null
            val name:String? = null
        }
        val author:String? = null
        var title:String? = null
        var description:String? = null
        val url:String? = null
        val urlToImage:String? = null
        val publishedAt:String? = null
        val content:String? = null
        var isLast = false
        var isFullInfo = false

        fun getDateWithWord():String{
            val date = publishedAt!!.removeRange(10,publishedAt.length)
            return Tools.simpleDateFormatToString(date)
        }

    }
}