package com.footballapp.app.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.callbacks.MatchRecyclerViewInterface
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.schedule_matches_rv_layout.view.*

class ScheduleMatchesRecyclerViewAdapter(private val matches: MutableList<MatchModel>, private val matchRecyclerViewInterface: MatchRecyclerViewInterface) :
    RecyclerView.Adapter<ScheduleMatchesRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.schedule_matches_rv_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = matches.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) , View.OnClickListener {
        private lateinit var model: MatchModel
        fun onBind(){
            model = matches[adapterPosition]
            itemView.dateLeagueMatchesTV.text = model.getDateWithWord()
            itemView.leagueHomeTeamTV.text = model.matchHomeTeamName
            itemView.leagueLocationMatchesTV.text = model.matchStadium
            itemView.leagueAwayTeamTV.text = model.matchAwayTeamName
            itemView.leagueHomeTeamImageView.setGlideImage(model.teamHomeBadge)
            itemView.leagueAwayTeamImageView.setGlideImage(model.teamAwayBadge)
//            if (model.matchStatus.contains("Finished"))
                itemView.setOnClickListener(this)
            if (model.matchLive.contains("1"))
                itemView.liveIndicatorTextView.visible(true)
            else
                itemView.liveIndicatorTextView.visible(false)


            if (!model.matchStatus.contains("Finished"))
                itemView.leagueMatchScoreOrTime.text = model.matchTime
            else
                itemView.leagueMatchScoreOrTime.text = model.getFullTimeScore()
        }

        override fun onClick(v: View?) {
            matchRecyclerViewInterface.onMatchClick(model)
        }
    }
}
