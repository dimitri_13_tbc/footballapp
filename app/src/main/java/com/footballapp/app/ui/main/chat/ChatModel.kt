package com.footballapp.app.ui.main.chat


class ChatModel {
    var id: String = ""
    var text: String = ""
    var profileImgUrl:String = ""
    var username:String = ""
    var favouriteClubUrl :String = ""
    var imageMsg: String = ""
    var time:String = ""
}

