package com.footballapp.app.ui.main.account.sign_up

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log.d
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.footballapp.app.R
import com.footballapp.app.databinding.ActivitySignUpBinding
import com.footballapp.app.tools.Tools
import com.footballapp.app.tools.extensions.setValidColor
import com.footballapp.app.tools.extensions.visible
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.HomeActivity.Companion.userActive
import com.footballapp.app.ui.main.account.UserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.progressbar_layout.*
import java.util.*

class SignUpActivity : AppCompatActivity() {
    private var selectedPhotoUri: Uri? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var storage: FirebaseStorage
    private lateinit var binding: ActivitySignUpBinding
    private lateinit var viewModel: SignUpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().reference
        storage = FirebaseStorage.getInstance()
        viewModel = ViewModelProvider(this)[SignUpViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        init()

    }

    private fun init() {
        signUpButton.setOnClickListener {
            signUp()
        }
        circleProfileImage.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/+"
            startActivityForResult(intent, 0)
        }
        setObservers()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {

            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            circleProfileImage.setImageBitmap(bitmap)
        }

    }

    private fun uploadImageToFirebaseStorage() {
        if (selectedPhotoUri == null) return

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {


                    saveUserToFirebaseDatabase(it.toString())
                }
            }
            .addOnFailureListener {
                spinKitContainer.visible(false)
            }
    }


    private fun signUp() {
        if (Tools.isEmailValid(viewModel.emailLiveData.value!!) && Tools.passwordValidation(
                viewModel.passwordLiveData.value!!
            ) && viewModel.repeatPasswordLiveData.value!! == viewModel.passwordLiveData.value!!
            && selectedPhotoUri != null
        ) {
            spinKitContainer.visible(true)
            auth.createUserWithEmailAndPassword(
                viewModel.emailLiveData.value!!,
                viewModel.passwordLiveData.value!!
            )
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {

                        uploadImageToFirebaseStorage()
                    } else {
                        spinKitContainer.visible(false)
                        Tools.initDialog(
                            this,
                            getString(R.string.registration_failed),
                            getString(R.string.please_try_again)
                        )
                    }
                }
        } else {
            Tools.initDialog(
                this,
                getString(R.string.fill_all_input),
                getString(R.string.all_input_green)
            )
        }


    }

    private fun saveUserToFirebaseDatabase(profileImageUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = UserModel()
        user.uid = uid
        user.email = viewModel.emailLiveData.value!!
        user.username = viewModel.userNameLiveData.value!!
        user.password = viewModel.passwordLiveData.value!!
        user.profileImageUrl = profileImageUrl

        ref.setValue(user)
            .addOnSuccessListener {
                val intent = Intent(this, HomeActivity::class.java)
                userActive = true
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                spinKitContainer.visible(false)
                startActivity(intent)
            }
    }

    private fun setObservers() {
        viewModel.emailLiveData.observe(this, androidx.lifecycle.Observer {
            emailSignUpEditText.setValidColor(Tools.isEmailValid(it))
        })
        viewModel.passwordLiveData.observe(this, androidx.lifecycle.Observer {
            passwordSignUpEditText.setValidColor(Tools.passwordValidation(it))
        })
        viewModel.repeatPasswordLiveData.observe(this, androidx.lifecycle.Observer {
            confirmPasswordSignUpEditText.setValidColor(viewModel.passwordLiveData.value == viewModel.repeatPasswordLiveData.value)
        })
        viewModel.userNameLiveData.observe(this, androidx.lifecycle.Observer {
            usernameSignUpEditText.setValidColor(true)
        })
    }
}