package com.footballapp.app.ui.main.events.tab_layout.stats.adapters.cards

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.event_away_player_info_recyclerview_item_layout.view.*
import kotlinx.android.synthetic.main.event_home_player_info_recyclerview_item_layout.view.*

class HomeCardsRecyclerViewAdapter(private val cards: MutableList<MatchModel.Cards>) :
    RecyclerView.Adapter<HomeCardsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_home_player_info_recyclerview_item_layout, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = cards.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: MatchModel.Cards
        fun onBind() {
            model = cards[adapterPosition]
            if (model.card.contains("yellow card"))
                itemView.homeCardImageView.setImageResource(R.mipmap.ic_card_yellow)
            else{
                itemView.homeCardImageView.setImageResource(R.mipmap.ic_card_red)
            }
            itemView.homeCardImageView.visible(true)
            itemView.homeTeamNumberShirtButton.visible(false)
            itemView.homeTeamScoreTimeTextView.text = model.getCardTime()
            itemView.homeTeamNameTextView.text = model.homeFault
        }
    }
}