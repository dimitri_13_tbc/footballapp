package com.footballapp.app.ui.countries

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.footballapp.app.App
import com.footballapp.app.R
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.FootballDataLoader
import com.footballapp.app.services.network.Endpoints
import com.footballapp.app.callbacks.CustomClickRV
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.ui.leagues.LeaguesActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_countries.*
import kotlinx.android.synthetic.main.choose_team_toolbar_layout.*
import kotlinx.android.synthetic.main.progressbar_layout.*

class CountriesActivity : AppCompatActivity() {

    private lateinit var adapter: CountriesRecyclerViewAdapter
    private val countriesList = mutableListOf<CountriesModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countries)
        init()
    }

    private fun init() {
        setSupportActionBar(toolbar)
        supportActionBar!!.title = getString(R.string.select_your_country)

        adapter = CountriesRecyclerViewAdapter(countriesList, onclick)
        countriesRecyclerView.layoutManager = GridLayoutManager(this, 3)
        countriesRecyclerView.adapter = adapter
        getCountries()
    }

    private val onclick = object :
        CustomClickRV {
        override fun itemClick(id: String) {
            toLeaguesActivity(id)
        }

    }

    private fun toLeaguesActivity(id: String) {
        val intent = Intent(this, LeaguesActivity::class.java)
        intent.putExtra("country_id", id)
        startActivity(intent)
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
    }


    private fun getCountries() {
        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_COUNTRIES
        parameters["APIkey"] = SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        FootballDataLoader.getRequest(spinKitContainer, parameters, object :
            CustomCallback {
            override fun onResponse(response: String) {
                val toList = Gson().fromJson(response, Array<CountriesModel>::class.java).toList()
                toList.forEach {
                    if (it.countryLogo.isNotEmpty()){
                        countriesList.add(it)
                        adapter.filterItems.add(it)
                    }

                }

                adapter.notifyDataSetChanged()

            }

            override fun onFailure(title: String, errorMessage: String) {

            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val item = menu!!.findItem(R.id.action_search)
        val searchView = item.actionView as SearchView
        val editText =
            searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        editText.hint = "Search"
        editText.setHintTextColor(ContextCompat.getColor(App.context!!,android.R.color.white))
        editText.setTextColor(ContextCompat.getColor(App.context!!,android.R.color.white))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean { return false }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

}