package com.footballapp.app.ui.main.competitions.tab_layouts.schedule


import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.FootballDataLoader
import com.footballapp.app.services.network.Endpoints
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.tools.Tools
import com.footballapp.app.ui.main.competitions.tab_layouts.schedule.current_matchday.CurrentMatchDayFragment
import com.footballapp.app.ui.main.competitions.tab_layouts.schedule.next_matchday.NextMatchDayFragment
import com.footballapp.app.ui.main.competitions.tab_layouts.schedule.previous_matchday.PreviousMatchDayFragment
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_schedule.view.*
import kotlinx.android.synthetic.main.progressbar_layout.*


class ScheduleFragment : BaseFragment() {

    override fun layoutResource() = R.layout.fragment_schedule

    companion object {
        val matchRoundsInt = mutableListOf<String>()
    }

    private val currentMatchDay = mutableListOf<MatchModel>()
    private val previousMatchDay = mutableListOf<MatchModel>()
    private val nextMatchDay = mutableListOf<MatchModel>()


    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        getMatchesSchedule()
    }

    private fun initTabLayout() {
        val fragments = mutableListOf<Fragment>()
        fragments.add(PreviousMatchDayFragment(previousMatchDay))
        fragments.add(CurrentMatchDayFragment(currentMatchDay))
        fragments.add(NextMatchDayFragment(nextMatchDay))

        itemView!!.scheduleViewPager.adapter =
            ScheduleViewPagerAdapter(childFragmentManager, fragments)
        itemView!!.scheduleTabLayout.setupWithViewPager(itemView!!.scheduleViewPager)
        itemView!!.scheduleViewPager.offscreenPageLimit = fragments.size
        itemView!!.scheduleTabLayout.setupWithViewPager(itemView!!.scheduleViewPager)

        itemView!!.scheduleTabLayout!!.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                itemView!!.scheduleViewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

    private fun getMatchesSchedule() {

        val currentDates = Tools.getCurrentDates()

        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_EVENTS
        parameters["APIkey"] =
            SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        parameters["from"] = currentDates.getValue("from")
        parameters["to"] = currentDates.getValue("to")
        parameters["league_id"] = SharedPreference.instance().getString(SharedPreference.LEAGUE)!!
        FootballDataLoader.getRequest(spinKitContainer, parameters, object : CustomCallback {
            override fun onResponse(response: String) {
                d("responseee", response)
                if (response[0] == '[') {
                    val matchModel =
                        Gson().fromJson(response, Array<MatchModel>::class.java).toList()

                    var currentRoundStr = ""
                    if (matchModel.size > 1) {
                        for (each in matchModel.indices) {
                            if (matchModel[each].matchStatus.isEmpty()) {
                                currentRoundStr = matchModel[each].matchRound
                                break
                            }
                        }
                        val lastRound: Int
                        val nextRound: Int
                        val currentRound = if (currentRoundStr.isNotEmpty()) {
                            currentRoundStr.split(" ")[1].toInt()
                        } else {
                            2
                        }
                        lastRound = currentRound - 1
                        nextRound = currentRound + 1
                        matchRoundsInt.add(lastRound.toString())
                        matchRoundsInt.add(currentRound.toString())
                        matchRoundsInt.add(nextRound.toString())
                        if (matchModel[0].matchRound.contains("Round")) {
                            matchModel.forEach {
                                d("responseee1", it.matchRound)
                                when {
                                    it.matchRound.split(" ")[1].toInt() == matchRoundsInt[0].toInt() -> previousMatchDay.add(
                                        it
                                    )
                                    it.matchRound.split(" ")[1].toInt() == matchRoundsInt[1].toInt() -> currentMatchDay.add(
                                        it
                                    )
                                    it.matchRound.split(" ")[1].toInt() == matchRoundsInt[2].toInt() -> nextMatchDay.add(
                                        it
                                    )
                                }
                            }
                        }
                        initTabLayout()
                    }
                }


            }

            override fun onFailure(title: String, errorMessage: String) {

            }
        })
    }

}