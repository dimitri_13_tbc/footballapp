package com.footballapp.app.ui.main.competitions.tab_layouts

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.footballapp.app.ui.main.competitions.tab_layouts.schedule.ScheduleFragment
import com.footballapp.app.ui.main.competitions.tab_layouts.standings.StandingsFragment

class CompetitionViewPagerAdapter(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ScheduleFragment()
            1 -> StandingsFragment()
            else -> ScheduleFragment()

        }
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Schedule"
            1 -> "Standings"
            else -> "Schedule"
        }
    }
}