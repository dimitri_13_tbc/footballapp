package com.footballapp.app.services.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiService {
    @GET("?")
    fun getFootballRequest(@QueryMap param: Map<String,String>?): Call<String>

    @GET("everything")
    fun getNewsRequest(@QueryMap param: Map<String,String>?): Call<String>

    @GET("search")
    fun getVideosRequest(@QueryMap param: Map<String,String>?):Call<String>

}