package com.footballapp.app.ui.main.competitions

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import com.footballapp.app.R
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.competitions.tab_layouts.CompetitionViewPagerAdapter
import com.footballapp.app.ui.main.competitions.tab_layouts.schedule.ScheduleFragment
import com.footballapp.app.ui.main.competitions.tab_layouts.standings.StandingsFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.choose_team_toolbar_layout.view.toolbarTV
import kotlinx.android.synthetic.main.fragment_competitions.view.*
import kotlinx.android.synthetic.main.main_toolbar_layout.view.*

class CompetitionsFragment : BaseFragment() {
    override fun layoutResource() = R.layout.fragment_competitions

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }

    private fun init() {
        itemView!!.toolbarTV.text = SharedPreference.instance().getString(SharedPreference.LEAGUE_NAME)
        itemView!!.toolbarImageView.setGlideImage(SharedPreference.instance().getString(SharedPreference.LEAGUE_LOGO)!!)

        val fragments = mutableListOf<Fragment>()
        fragments.add(ScheduleFragment())
        fragments.add(StandingsFragment())
        itemView!!.competitionsViewPager.adapter =
            CompetitionViewPagerAdapter(
                childFragmentManager
            )
        itemView!!.competitionsViewPager.offscreenPageLimit = fragments.size
        itemView!!.competitionsTabLayout.setupWithViewPager(itemView!!.competitionsViewPager)

        itemView!!.competitionsTabLayout!!.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                itemView!!.competitionsViewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

}