package com.footballapp.app.ui.main.account.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LogInViewModel : ViewModel() {
    val emailLiveData by lazy {
        MutableLiveData<String>()
    }

    val passwordLiveData by lazy {
        MutableLiveData<String>()
    }
    private val isUserActive by lazy {
        MutableLiveData<Boolean>()
    }
    init {
        emailLiveData.value = ""
        passwordLiveData.value = ""
        isUserActive.value = false
    }
}