package com.footballapp.app.ui.main.competitions.tab_layouts.schedule

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ScheduleViewPagerAdapter(fm: FragmentManager, private val fragments: MutableList<Fragment>) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount() = 3

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "MatchDay " + ScheduleFragment.matchRoundsInt[0]
            1 -> "MatchDay " + ScheduleFragment.matchRoundsInt[1]
            2 -> "MatchDay " + ScheduleFragment.matchRoundsInt[2]
            else -> "MatchDay " + ScheduleFragment.matchRoundsInt[0]
        }
    }
}