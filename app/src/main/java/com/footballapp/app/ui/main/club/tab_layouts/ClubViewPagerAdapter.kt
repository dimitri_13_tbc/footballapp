package com.footballapp.app.ui.main.club.tab_layouts

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.footballapp.app.ui.main.club.tab_layouts.players.PlayersFragment
import com.footballapp.app.ui.main.club.tab_layouts.schedule.ClubScheduleFragment
import com.footballapp.app.ui.main.club.tab_layouts.team.TeamFragment
import com.footballapp.app.ui.main.competitions.tab_layouts.schedule.ScheduleFragment

class ClubViewPagerAdapter(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> TeamFragment()
            1 -> ClubScheduleFragment()
            2 -> PlayersFragment()
            else -> ScheduleFragment()

        }
    }

    override fun getCount() = 3

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Club"
            1 -> "Schedule"
            2 -> "Players"
            else -> "Club"
        }
    }
}