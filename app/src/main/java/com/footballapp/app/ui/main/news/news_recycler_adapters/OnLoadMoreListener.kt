package com.footballapp.app.ui.main.news.news_recycler_adapters

interface OnLoadMoreListener {
    fun onLoadMore()
}