package com.footballapp.app.tools.extensions

import android.view.View

fun View.visible(boolean: Boolean){
    visibility = if (boolean) View.VISIBLE
    else View.GONE
}