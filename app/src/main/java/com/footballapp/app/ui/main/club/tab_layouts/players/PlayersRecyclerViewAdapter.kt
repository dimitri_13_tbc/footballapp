package com.footballapp.app.ui.main.club.tab_layouts.players

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.ui.clubs.ClubModel
import kotlinx.android.synthetic.main.news_recyclerview_item_layout.view.*
import kotlinx.android.synthetic.main.players_recyclerview_header_item_layout.view.*
import kotlinx.android.synthetic.main.players_recyclerview_header_item_layout.view.positionTextView
import kotlinx.android.synthetic.main.players_recyclerview_player_item_layout.view.*

class PlayersRecyclerViewAdapter(
    private val clubModel: ClubModel,
    private val goalKeeper: Int,
    private val defender: Int,
    private val midfielder: Int,
    private val forward: Int
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val HEAD_VIEW_TYPE = 1
        private const val ITEM_VIEW_TYPE = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEAD_VIEW_TYPE -> HeadViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.players_recyclerview_header_item_layout, parent, false)
            )
            else -> ItemViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.players_recyclerview_player_item_layout, parent, false)
            )
        }
    }

    override fun getItemCount() = clubModel.players.size + clubModel.coaches.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is ItemViewHolder) {
            holder.onBind()
        } else if (holder is HeadViewHolder) {
            holder.onBind()
        }
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var playerModel: ClubModel.Players
        fun onBind() {
            if (adapterPosition < clubModel.players.size) {
                playerModel = clubModel.players[adapterPosition]

                itemView.playerNameTextView.text = playerModel.playerName
                itemView.playerNameTextView2.text = playerModel.playerName.split(" ")[0]
                itemView.goalTextView.text = playerModel.playerGoals
                itemView.yellowCardTextView.text = playerModel.playerYellowCards
                itemView.redCardTextView.text = playerModel.playerRedCards
                itemView.playerNumberTextView.text = playerModel.playerNumber
                itemView.playerNumberTextView2.text = playerModel.playerNumber
                itemView.ageTextView2.text = playerModel.playerAge
                itemView.gamePlayedTextView2.text = playerModel.playerMatchPlayed
                itemView.countryTextView.text = playerModel.playerCountry
                itemView.teamLogoImageView2.setGlideImage(clubModel.teamBadge)
                itemView.playersDetailInfo.visibility =
                    if (playerModel.isFullInfo) View.VISIBLE else View.GONE

                itemView.setOnClickListener {
                    playerModel.isFullInfo = !playerModel.isFullInfo
                    notifyItemChanged(adapterPosition)
                }
            }


        }
    }

    inner class HeadViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var playerModel: ClubModel.Players
        private lateinit var coachModel: ClubModel.Coaches
        fun onBind() {
            if (adapterPosition < clubModel.players.size) {
                playerModel = clubModel.players[adapterPosition]
                itemView.positionTextView.text = playerModel.playerType
                itemView.playersDetailInfo.visibility =
                    if (playerModel.isFullInfo) View.VISIBLE else View.GONE
                itemView.setOnClickListener {
                    playerModel.isFullInfo = !playerModel.isFullInfo
                    notifyItemChanged(adapterPosition)
                }
                if (playerModel.playerType.contains("Goalkeepers"))
                    itemView.positionIconImage.setImageResource(R.mipmap.ic_gloves)
                else
                    itemView.positionIconImage.setImageResource(R.mipmap.tshirt_all)

                itemView.playerNameTextView.text = playerModel.playerName
                itemView.playerNameTextView2.text = playerModel.playerName.split(" ")[0]
                itemView.goalTextView.text = playerModel.playerGoals
                itemView.yellowCardTextView.text = playerModel.playerYellowCards
                itemView.redCardTextView.text = playerModel.playerRedCards
                itemView.playerNumberTextView.text = playerModel.playerNumber
                itemView.playerNumberTextView2.text = playerModel.playerNumber
                itemView.ageTextView2.text = playerModel.playerAge
                itemView.gamePlayedTextView2.text = playerModel.playerMatchPlayed
                itemView.countryTextView.text = playerModel.playerCountry
                itemView.teamLogoImageView2.setGlideImage(clubModel.teamBadge)
                itemView.playerNumberTextView.visibility = View.VISIBLE
//                itemView.playerNumberTextView.visibility = View.VISIBLE
//                itemView.yellowCardTextView.visibility = View.VISIBLE
//                itemView.redCardTextView.visibility = View.VISIBLE
//                itemView.goalTextViewView.visibility = View.VISIBLE
//                itemView.ballImageView.visibility = View.VISIBLE
//                itemView.yellowCard.visibility = View.VISIBLE
//                itemView.redCard.visibility = View.VISIBLE

            } else {
                coachModel = clubModel.coaches[clubModel.coaches.size - 1]
                itemView.positionTextView.text = itemView.context.getString(R.string.coach)
                itemView.isClickable = false
//                itemView.ballImageView.visibility = View.INVISIBLE
//                itemView.yellowCard.visibility = View.INVISIBLE
//                itemView.redCard.visibility = View.INVISIBLE
                itemView.playerNameTextView.text = coachModel.coachName
                itemView.positionIconImage.setImageResource(R.mipmap.ic_tactic)
                itemView.playerNumberTextView.visibility = View.GONE
//                itemView.yellowCardTextView.visibility = View.INVISIBLE
//                itemView.redCardTextView.visibility = View.INVISIBLE
//                itemView.goalTextViewView.visibility = View.INVISIBLE
            }


        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0 || position == goalKeeper ||
            position == defender + goalKeeper ||
            position == defender + goalKeeper + midfielder ||
            position == defender + goalKeeper + midfielder + forward
        )
            HEAD_VIEW_TYPE
        else
            ITEM_VIEW_TYPE

    }
}