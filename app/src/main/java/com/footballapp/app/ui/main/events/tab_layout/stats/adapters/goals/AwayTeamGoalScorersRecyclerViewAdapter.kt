package com.footballapp.app.ui.main.events.tab_layout.stats.adapters.goals

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.visible
import kotlinx.android.synthetic.main.event_away_player_info_recyclerview_item_layout.view.*

class AwayTeamGoalScorersRecyclerViewAdapter(private val items:List<MatchModel.Goalscorer>) : RecyclerView.Adapter<AwayTeamGoalScorersRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.event_away_player_info_recyclerview_item_layout, parent, false)
        )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: MatchModel.Goalscorer
        fun onBind(){
            model = items[adapterPosition]
            itemView.awayTeamNameTextView.text = model.awayScorer
            itemView.awayTeamScoreTimeTextView.text = model.getGoalTime()
            itemView.awayTeamNumberShirtButton.visible(false)
            itemView.awayBallImageView.visible(true)

        }
    }
}