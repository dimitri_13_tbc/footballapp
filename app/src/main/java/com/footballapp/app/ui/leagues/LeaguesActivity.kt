package com.footballapp.app.ui.leagues

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.footballapp.app.App
import com.footballapp.app.R
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.FootballDataLoader
import com.footballapp.app.services.network.Endpoints
import com.footballapp.app.callbacks.CustomClickRV
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.ui.clubs.ClubsActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_leagues.*
import kotlinx.android.synthetic.main.choose_team_toolbar_layout.*
import kotlinx.android.synthetic.main.progressbar_layout.*

class LeaguesActivity : AppCompatActivity() {

    private lateinit var adapter: LeaguesRecyclerViewAdapter
    private val leaguesList = mutableListOf<LeaguesModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_leagues)

        init()

    }

    private fun init() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbarTV.text = getString(R.string.select_league)

        adapter = LeaguesRecyclerViewAdapter(leaguesList, onAdapterClick)
        leaguesRecyclerView.layoutManager = GridLayoutManager(this, 3)
        leaguesRecyclerView.adapter = adapter
        val countryId = intent.extras!!.getString("country_id")
        getLeagues(countryId!!)
    }

    private val onAdapterClick = object :
        CustomClickRV {
        override fun itemClick(id: String) {
            SharedPreference.instance().saveString(SharedPreference.LEAGUE,id)
            leaguesList.forEach {
                if (it.leagueId == id){
                    SharedPreference.instance().saveString(SharedPreference.LEAGUE_NAME,it.leagueName)
                    SharedPreference.instance().saveString(SharedPreference.LEAGUE_LOGO,it.leagueLogo)
                }
            }
            toClubsActivity(id)


        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)

    }

    private fun toClubsActivity(id:String){
        val intent = Intent(this,ClubsActivity::class.java)
        intent.putExtra("league_id",id)
        startActivity(intent)
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
    }

    private fun getLeagues(id: String) {
        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_LEAGUES
        parameters["country_id"] = id
        parameters["APIkey"] = SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        FootballDataLoader.getRequest(spinKitContainer, parameters, object :
            CustomCallback {
            override fun onResponse(response: String) {
                val list = Gson().fromJson(response, Array<LeaguesModel>::class.java).toList()
                list.forEach {
                    leaguesList.add(it)
                    adapter.filterItems.add(it)
                    adapter.notifyDataSetChanged()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val item = menu!!.findItem(R.id.action_search)
        val searchView = item.actionView as SearchView
        val editText =
            searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        editText.hint = "Search"
        editText.setHintTextColor(ContextCompat.getColor(App.context!!,android.R.color.white))
        editText.setTextColor(ContextCompat.getColor(App.context!!,android.R.color.white))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean { return false }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

}