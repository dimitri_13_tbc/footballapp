package com.footballapp.app.ui.clubs

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.footballapp.app.App
import com.footballapp.app.R
import com.footballapp.app.callbacks.CustomClickRV
import com.footballapp.app.services.network.CustomCallback
import com.footballapp.app.services.network.Endpoints
import com.footballapp.app.services.network.FootballDataLoader
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.tools.Tools
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.splash.GetApiKeyModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_clubs.*
import kotlinx.android.synthetic.main.choose_team_toolbar_layout.*
import kotlinx.android.synthetic.main.progressbar_layout.*
import org.json.JSONArray
import org.json.JSONObject


class ClubsActivity : AppCompatActivity() {

    private lateinit var adapter: ClubsRecyclerViewAdapter
    private val clubsList = mutableListOf<ClubsModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clubs)
        init()
    }

    private fun init() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbarTV.text = getString(R.string.select_your_club)
        adapter = ClubsRecyclerViewAdapter(clubsList, onClubClick)
        clubsRecyclerView.layoutManager = GridLayoutManager(this, 3)
        clubsRecyclerView.adapter = adapter
        val leagueId = intent.extras!!.getString("league_id", "")
        getAllClubs(leagueId)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
        }
        return true

    }

    private val onClubClick = object :
        CustomClickRV {
        override fun itemClick(id: String) {
            getClubInfo(id)
        }
    }

    private fun toBottomNavigation() {
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)

    }


    private fun getAllClubs(id: String) {
        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_TEAMS
        parameters["league_id"] = id
        parameters["APIkey"] = SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        d("apiKey222",SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!)
        FootballDataLoader.getRequest(spinKitContainer, parameters, object :
            CustomCallback {
            override fun onResponse(response: String) {
                val list = Gson().fromJson(response, Array<ClubsModel>::class.java).toList()
                list.forEach {
                    clubsList.add(it)
                    adapter.filterItems.add(it)
                    adapter.notifyDataSetChanged()
                }
            }
        })
    }

    fun getClubInfo(id: String) {
        val parameters = mutableMapOf<String, String>()
        parameters["action"] = Endpoints.GET_TEAMS
        parameters["team_id"] = id
        parameters["APIkey"] = SharedPreference.instance().getString(SharedPreference.FOOTBALL_API_KEY)!!
        FootballDataLoader.getRequest(spinKitContainer, parameters, object : CustomCallback {
            override fun onResponse(response: String) {
                val club = Gson().fromJson(response, Array<ClubModel>::class.java).toList()
                SharedPreference.instance().saveString(SharedPreference.CLUB, id)
                HomeActivity.clubModel = club[0]
                toBottomNavigation()
//                } else if (obj is JSONObject) {
//                    SharedPreference.instance().clear()
//                    Tools.initDialog(this@ClubsActivity,"No information","Sorry but there is no info for this Club at this time")
//                }

            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val item = menu!!.findItem(R.id.action_search)
        val searchView = item.actionView as SearchView
        val editText =
            searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        editText.hint = "Search"
        editText.setHintTextColor(ContextCompat.getColor(App.context!!, android.R.color.white))
        editText.setTextColor(ContextCompat.getColor(App.context!!, android.R.color.white))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

}
