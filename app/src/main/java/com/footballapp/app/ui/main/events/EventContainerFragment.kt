package com.footballapp.app.ui.main.events

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.FragmentManager
import com.footballapp.app.R
import com.footballapp.app.models.MatchModel
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.events.tab_layout.lineup.EventLineupFragment
import com.footballapp.app.ui.main.events.tab_layout.stats.EventStatisticFragment
import kotlinx.android.synthetic.main.activity_bottom_navigation.*
import kotlinx.android.synthetic.main.fragment_event_container.view.*


class EventContainerFragment(private val matchModel: MatchModel) : BaseFragment() {

    private lateinit var adapter: EventContainerViewPagerAdapter
    private lateinit var homeActivity: HomeActivity
    override fun layoutResource()  = R.layout.fragment_event_container

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {

        homeActivity = activity as HomeActivity
        init()
        initToolbar()
        backClick()
    }
    private fun backClick() {
        homeActivity.onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    homeActivity.viewPager.visibility = View.VISIBLE
                    homeActivity.fragmentContainer.visibility = View.GONE
                    homeActivity.supportFragmentManager.popBackStack(
                        "EVENT",
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )

                }

            })
    }

    private fun init(){
        val fragmentList = listOf<Fragment>(EventStatisticFragment(matchModel),
            EventLineupFragment(
                matchModel
            )
        )
        adapter = EventContainerViewPagerAdapter(fragmentList,childFragmentManager)
        itemView!!.eventStatsAndLineupViewPager.adapter = adapter
        itemView!!.eventStatsAndLineupTabLayout.setupWithViewPager(itemView!!.eventStatsAndLineupViewPager)
    }
    private fun initToolbar(){
        itemView!!.toolbarFirstTeamImageView.setGlideImage(matchModel.teamHomeBadge)
        itemView!!.toolbarFirstTeamScoreTextView.text = matchModel.matchHomeTeamScore
        itemView!!.toolbarMatchStatusTextView.text = matchModel.matchStatus
        itemView!!.toolbarSecondTeamScoreTextView.text = matchModel.matchAwayTeamScore
        itemView!!.toolbarSecondTeamImageView.setGlideImage(matchModel.teamAwayBadge)
    }


}