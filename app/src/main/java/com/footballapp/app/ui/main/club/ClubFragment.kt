package com.footballapp.app.ui.main.club

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import com.footballapp.app.R
import com.footballapp.app.services.shared_pref.SharedPreference
import com.footballapp.app.tools.extensions.setGlideImage
import com.footballapp.app.tools.extensions.visible
import com.footballapp.app.ui.BaseFragment
import com.footballapp.app.ui.countries.CountriesActivity
import com.footballapp.app.ui.main.HomeActivity
import com.footballapp.app.ui.main.club.tab_layouts.ClubViewPagerAdapter
import com.footballapp.app.ui.main.club.tab_layouts.players.PlayersFragment
import com.footballapp.app.ui.main.club.tab_layouts.schedule.ClubScheduleFragment
import com.footballapp.app.ui.main.club.tab_layouts.team.TeamFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.choose_team_toolbar_layout.view.toolbarTV
import kotlinx.android.synthetic.main.fragment_club.view.*
import kotlinx.android.synthetic.main.main_toolbar_layout.view.*

class ClubFragment : BaseFragment() {


    override fun layoutResource() = R.layout.fragment_club

    override fun setInit(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
    }
    private fun init() {

        initClubViewPager()
        initToolbar()

    }

    private fun initClubViewPager(){
        val fragments = mutableListOf<Fragment>()
        fragments.add(PlayersFragment())
        fragments.add(ClubScheduleFragment())
        fragments.add(TeamFragment())

        itemView!!.clubViewPager.adapter =
            ClubViewPagerAdapter(
                childFragmentManager
            )
        itemView!!.clubViewPager.offscreenPageLimit = fragments.size
        itemView!!.clubTabLayout.setupWithViewPager(itemView!!.clubViewPager)

        itemView!!.clubTabLayout!!.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                itemView!!.clubViewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }


    private fun initToolbar(){
        itemView!!.changeClubButton.visible(true)
        itemView!!.changeClubButton.setOnClickListener {
            SharedPreference.instance().clear()
            val intent = Intent(context,CountriesActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)

        }
        itemView!!.toolbarTV.text = HomeActivity.clubModel.teamName
        itemView!!.toolbarImageView.setGlideImage(HomeActivity.clubModel.teamBadge)
    }

}