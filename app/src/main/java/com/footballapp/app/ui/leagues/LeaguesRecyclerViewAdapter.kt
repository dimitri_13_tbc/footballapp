package com.footballapp.app.ui.leagues

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.callbacks.CustomClickRV
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.main_recyclerview_item_layout.view.*
import java.util.*

class LeaguesRecyclerViewAdapter(private val items: MutableList<LeaguesModel>,val customClickRV: CustomClickRV) :
    RecyclerView.Adapter<LeaguesRecyclerViewAdapter.ViewHolder>(),Filterable {
    var filterItems = mutableListOf<LeaguesModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.main_recyclerview_item_layout, parent, false
        )
    )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: LeaguesModel
        fun onBind() {
            model = items[adapterPosition]
            Glide.with(itemView.context).load(model.leagueLogo).into(itemView.logoImageView)
            itemView.titleTextView.text = model.leagueName
            itemView.setOnClickListener {
                customClickRV.itemClick(model.leagueId)
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val resultList = mutableListOf<LeaguesModel>()
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    resultList.addAll(filterItems)
                } else {
                    for (item in filterItems) {
                        if (item.leagueName.toLowerCase(Locale.ROOT).contains(
                                charSearch.toLowerCase(
                                    Locale.ROOT
                                )
                            )
                        ) {
                            resultList.add(item)
                        }
                    }

                }
                val filterResults = FilterResults()
                filterResults.values = resultList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                items.clear()
                items.addAll(results!!.values as Collection<LeaguesModel>)
                notifyDataSetChanged()
            }

        }
    }
}