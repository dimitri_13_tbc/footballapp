package com.footballapp.app.ui.main.events.tab_layout.stats.adapters.statistic

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.footballapp.app.R
import com.footballapp.app.ui.main.events.tab_layout.stats.EventStatisticFragment
import kotlinx.android.synthetic.main.match_timeline_recycklerview_item_layout.view.*

class MatchTimelineRecyclerViewAdapter(private val items:MutableList<MatchTimeLineModel>) : RecyclerView.Adapter<MatchTimelineRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(
        R.layout.match_timeline_recycklerview_item_layout,parent,false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        lateinit var model: MatchTimeLineModel
        fun onBind(){
            model = items[adapterPosition]
            itemView.timeTextView.text = model.getGoalTime()
            if (model.home){
                itemView.homeTeamPlayerTextView.text = model.player
                when (model.action) {
                    EventStatisticFragment.GOAL -> itemView.homeTeamImageView.setImageResource(R.mipmap.ic_ball)
                    EventStatisticFragment.YELLOW_CARD -> itemView.homeTeamImageView.setImageResource(R.mipmap.ic_card_yellow)
                    EventStatisticFragment.RED_CARD -> itemView.homeTeamImageView.setImageResource(R.mipmap.ic_card_red)
                    else -> itemView.homeTeamImageView.setImageResource(R.mipmap.ic_event_subtitutes)
                }
            } else {
                itemView.awayTeamPlayerTextView.text = model.player
                when (model.action) {
                    EventStatisticFragment.GOAL -> itemView.awayTeamImageView.setImageResource(R.mipmap.ic_ball)
                    EventStatisticFragment.YELLOW_CARD -> itemView.awayTeamImageView.setImageResource(R.mipmap.ic_card_yellow)
                    EventStatisticFragment.RED_CARD -> itemView.awayTeamImageView.setImageResource(R.mipmap.ic_card_red)
                    else -> itemView.awayTeamImageView.setImageResource(R.mipmap.ic_event_subtitutes)
                }
            }
            if (adapterPosition % 2 == 1)
                itemView.setBackgroundColor(ContextCompat.getColor(itemView.context,android.R.color.transparent))
            else
                itemView.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.transparentGray))


        }
    }
}